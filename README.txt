How to run the program.

% cd src/psh/
% make
% python3 ./nyannyan.py

make builds all necessary binaries, and ./nyannyan.py is a script to create
a solution. It creates a tmp directory under /tmp/nyan2.${timestamp}/,
and also creates a solution archive /tmp/nyan2.${timestamp}.zip.

Meta solution approach.
./nyannyan.py reads .mdl files, and creates groups of the same file.
For each file group, run assemble solvers.
If the file group contains a source file, it creates the disassemble
solution by reversing the "best" assemble solution.
Also, it runs disassemble specific solvers, too, if necessary.

Solution.
- Assemble problem: find the best assemble solution.
- Disassemble problem: find the best disassemble solution.
- Reassemble problem: find the best disassemble solution for the source file,
  and fine the best assemble solution for the target file.
  Then concat those two with removing last Halt command in disassemble
  solution.

Solvers:
We run multiple solvers. For assembling,
- default: Take a bouding box of the model. Run scanline.
- dc: Split the bounding box in x-/z- axis. Do not split in y-axis.
      Solve each boxes.
- dc2: Almost as same as dc, but per scan line, look at 3 lines at once.
- ki: Each bot parallelly/greedily/repeatedly fills the nearest/bottommost
      grounded target.

For disassembling,
- box: If bounding box is smaller than 30x30x30, use GVoid.
- reverse_trace: Run the assembling solvers and semantically reverse the trace.

Reassembling is solved as disassembling followed by assembling.
