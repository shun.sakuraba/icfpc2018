#ifndef MODEL_UTIL_H_
#define MODEL_UTIL_H_

namespace model_util {

inline bool HasFilledPixelInLine(
    const Model& model, int y, int z, int min_x, int max_x) {
  for (int x = min_x; x < max_x; ++x) {
    if (model.filled(Vec3{x, y, z}))
      return true;
  }
  return false;
}

inline bool HasFilledPixelInLine(const Model& model, int y, int z) {
  return HasFilledPixelInLine(model, y, z, 0, model.r());
}

inline bool HasFilledPixelInPlane(
    const Model& model, int y, int min_z, int max_z, int min_x, int max_x) {
  for (int z = min_z; z < max_z; ++z) {
    if (HasFilledPixelInLine(model, y, z, min_x, max_x))
      return true;
  }
  return false;
}

inline bool HasFilledPixelInPlane(const Model& model, int y) {
  return HasFilledPixelInPlane(model, y, 0, model.r(), 0, model.r());
}

inline std::tuple<int, int> FindMinMaxInPlane(
    const Model& model, int y, int min_z, int max_z, int min_x, int max_x) {
  int min = -1;
  int max = -1;
  for (int z = min_z; z < max_z; ++z) {
    if (HasFilledPixelInLine(model, y, z, min_x, max_x)) {
      if (min == -1) min = z;
      max = z;
    }
  }
  return std::make_tuple(min, max);
}

inline std::tuple<int, int> FindMinMaxInPlane(const Model& model, int y) {
  return FindMinMaxInPlane(model, y, 0, model.r(), 0, model.r());
}

inline std::tuple<int, int> FindMinMaxInLine(
    const Model& model, int y, int z, int min_x, int max_x) {
  int min = -1;
  int max = -1;
  for (int x = min_x; x < max_x; ++x) {
    if (model.filled(Vec3{x, y, z})) {
      if (min == -1) min = x;
      max = x;
    }
  }
  return std::make_tuple(min, max);
}

inline std::tuple<int, int> FindMinMaxInLine(
    const Model& model, int y, int z) {
  return FindMinMaxInLine(model, y, z, 0, model.r());
}

}  // namespace model_util.

#endif  // MODEL_UTIL_H_
