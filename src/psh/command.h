#ifndef COMMAND_H_
#define COMMAND_H_

#include "vec3.h"

#include <tuple>
#include <vector>
#include <string>

class Command {
 public:
  enum class Type {
    Halt,
    Wait,
    Flip,
    SMove,
    LMove,
    FusionP,
    FusionS,
    Fission,
    Fill,
    Void,
    GFill,
    GVoid,
  };

  Command() = default;
  ~Command() = default;

  const Type type() const { return type_; }
  const Vec3& arg1() const { return arg1_; }
  const Vec3& arg2() const { return arg2_; }
  int m() const { return arg2_.x; }  // For Fission.

  // Returns little endian.
  std::tuple<std::uint32_t, int> Encode() const;

  static Command Halt();
  static Command Wait();
  static Command Flip();
  static Command SMove(int x, int y, int z);
  static Command LMove(int x1, int y1, int z1, int x2, int y2, int z2);
  static Command FusionP(int x, int y, int z);
  static Command FusionS(int x, int y, int z);
  static Command Fission(int x, int y, int z, std::uint8_t m);
  static Command Fill(int x, int y, int z);
  static Command Void(int x, int y, int z);
  static Command GFill(int x1, int y1, int z1, int x2, int y2, int z2);
  static Command GVoid(int x1, int y1, int z1, int x2, int y2, int z2);

  // Vec3 argument versions
  static Command SMove(const Vec3& arg1) {
    return SMove(arg1.x, arg1.y, arg1.z);
  }
  static Command LMove(const Vec3& arg1, const Vec3& arg2) {
    return LMove(arg1.x, arg1.y, arg1.z, arg2.x, arg2.y, arg2.z);
  }
  static Command FusionP(const Vec3& arg1) { 
    return FusionP(arg1.x, arg1.y, arg1.z);
  }
  static Command FusionS(const Vec3& arg1) {
    return FusionS(arg1.x, arg1.y, arg1.z);
  }
  static Command Fission(const Vec3& arg1, std::uint8_t m) {
    return Fission(arg1.x, arg1.y, arg1.z, m);
  }
  static Command Fill(const Vec3& arg1) { 
    return Fill(arg1.x, arg1.y, arg1.z);
  }
  static Command Void(const Vec3& arg1) {
    return Void(arg1.x, arg1.y, arg1.z);
  }
  static Command GFill(const Vec3& arg1, const Vec3& arg2) {
    return GFill(arg1.x, arg1.y, arg1.z, arg2.x, arg2.y, arg2.z);
  }
  static Command GVoid(const Vec3& arg1, const Vec3& arg2) {
    return GVoid(arg1.x, arg1.y, arg1.z, arg2.x, arg2.y, arg2.z);
  }

 private:
  Type type_;
  Vec3 arg1_;
  Vec3 arg2_;
};

std::ostream& operator<<(std::ostream& os, const Command& command);

bool OutputTrace(const std::vector<Command>& trace,
                 const std::string& path);

// In case of error, empty vector is returned.
std::vector<Command> ParseTrace(const std::string& path);

#endif  // COMMAND_H_
