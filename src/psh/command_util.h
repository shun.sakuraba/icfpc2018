#ifndef COMMAND_UTIL_H_
#define COMMAND_UTIL_H_

#include <vector>

#include "command.h"

namespace command_util {

void MoveX(int dx, std::vector<Command>* trace);
void MoveY(int dy, std::vector<Command>* trace);
void MoveZ(int dz, std::vector<Command>* trace);

void Merge(const std::vector<std::vector<Command>>& input,
           std::vector<Command>* output);

}  // namespace command_util

#endif  // COMMAND_UTIL_H_
