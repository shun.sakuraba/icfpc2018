#ifndef BOT_H_
#define BOT_H_

#include <vector>
#include <utility>

#include "vec3.h"

class Bot {
 public:
  Bot(int bid, const Vec3& pos, std::vector<int> seeds)
      : bid_(bid),
        pos_(pos),
        seeds_(std::move(seeds)) {}

  int bid() const { return bid_; }
  const Vec3& pos() const { return pos_; }
  const std::vector<int>& seeds() const { return seeds_; }
  std::vector<int>* mutable_seeds() { return &seeds_; }

  void Move(const Vec3& d) { pos_ += d; }

 private:
  int bid_;
  Vec3 pos_;
  std::vector<int> seeds_;
};

inline std::ostream& operator<<(std::ostream& os, const Bot& bot) {
  os << "Bot{" << bot.bid() << ", " << bot.pos() << ", {";
  bool first = true;
  for (int seed : bot.seeds()) {
    if (first) {
      first = false;
    } else {
      os << ", ";
    }
    os << seed;
  }
  return os << "}}";
}

#endif  // BOT_H_
