#ifndef DEFAULT_SOLVER_H_
#define DEFAULT_SOLVER_H_

#include "model.h"
#include "command.h"

class DefaultSolver {
 public:
  explicit DefaultSolver(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;

  DefaultSolver(const DefaultSolver&) = delete;
  DefaultSolver& operator=(const DefaultSolver&) = delete;
};

#endif  // DEFAULT_SOLVER_H_
