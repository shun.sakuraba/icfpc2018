#ifndef VEC3_H_
#define VEC3_H_

#include <cstdint>
#include <tuple>
#include <ostream>
#include <algorithm>

struct Vec3 {
  int x = 0;
  int y = 0;
  int z = 0;

  // Vec3() : x(0), y(0), z(0) {}
  // => Vec3{}
  // Vec3(int xx, int yy, int zz) : x(xx), y(yy), z(zz) {}
  // => Vec3{xx, yy, zz}

  inline Vec3& operator+=(const Vec3& other) {
    x += other.x;
    y += other.y;
    z += other.z;
    return *this;
  }

  inline Vec3& operator-=(const Vec3& other) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
    return *this;
  }

  inline bool IsValid(int r) const {
    return 0 <= x && x < r && 0 <= y && y < r && 0 <= z && z < r;
  }
};

inline bool operator==(const Vec3& lhs, const Vec3& rhs) {
  return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

inline bool operator!=(const Vec3& lhs, const Vec3& rhs) {
  return !(lhs == rhs);
}

inline bool operator<(const Vec3& lhs, const Vec3& rhs) {
  return std::tie(lhs.x, lhs.y, lhs.z) < std::tie(rhs.x, rhs.y, rhs.z);
}

inline Vec3 operator+(const Vec3& lhs, const Vec3& rhs) {
  return Vec3(lhs) += rhs;
}

inline Vec3 operator-(const Vec3& lhs, const Vec3& rhs) {
  return Vec3(lhs) -= rhs;
}

inline Vec3 operator/(const Vec3& arg, int f) {
  return {arg.x/f, arg.y/f, arg.z/f};
}

inline Vec3 operator*(const Vec3& arg, int f) {
  return {arg.x*f, arg.y*f, arg.z*f};
}

inline Vec3 operator*(int f, const Vec3& arg) {
  return arg*f;
}

inline Vec3 operator-(const Vec3& arg) {
  return arg*-1;
}

inline std::ostream& operator<<(std::ostream& os, const Vec3& v) {
  return os << "{" << v.x << ", " << v.y << ", " << v.z << "}";
}

inline int MLen(const Vec3& v) {
  return std::abs(v.x) + std::abs(v.y) + std::abs(v.z);
}

inline int CLen(const Vec3& v) {
  return std::max({std::abs(v.x), std::abs(v.y), std::abs(v.z)});
}

inline bool IsNear(const Vec3& v) {
  return CLen(v)==1 && MLen(v)<2;
}

#endif  // VEC3_H_
