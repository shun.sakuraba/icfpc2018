#ifndef DC_SOLVER2_H_
#define DC_SOLVER2_H_

#include "model.h"
#include "command.h"

class DCSolver2 {
 public:
  explicit DCSolver2(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;
};


#endif  // DC_SOLVER2_H_
