#include "ufmatrix.h"

#include <cassert>
#include <iostream>
#include <set>
#include <deque>

namespace {

constexpr bool kIsDebug = false;

constexpr Vec3 neighbors[] = {
  {-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}, {0, 0, -1}, {0, 0, 1},
};

inline int ToIndex(const Vec3& pos, int r) {
  return pos.x * r * r + pos.y * r + pos.z + 1;
}

Vec3 ToVec(int index, int r) {
  --index;
  int z = index % r;
  index /= r;
  int y = index % r;
  int x = index / r;
  return Vec3{x, y, z};
}

bool AreNeighborCellsConnected(const std::vector<bool>& fulled,
                               int r,
                               const Vec3& pos) {
  std::set<int> goals;
  std::set<int> visited;
  std::deque<Vec3> q;

  for (const auto& dp : neighbors) {
    Vec3 p = pos + dp;
    if (!p.IsValid(r)) continue;
    int index = ToIndex(p, r);
    if (!fulled[index]) continue;

    if (q.empty()) {
      q.push_back(p);
      visited.insert(index);
    } else {
      goals.insert(index);
    }
  }

  while (!q.empty() && !goals.empty()) {
    Vec3 p = q.front();
    q.pop_front();
    for (const auto& dp : neighbors) {
      Vec3 next = p + dp;
      if (!next.IsValid(r)) continue;
      int index = ToIndex(next, r);
      if (!fulled[index] || MLen(next - pos) > 40)
        continue;
      goals.erase(index);
      if (!visited.insert(index).second)
        continue;
      q.push_back(next);
    }
  }

  return goals.empty();
}

}  // namespace

UFMatrix::UFMatrix(int r) :
    r_(r), fulled_(r * r * r + 1, false),
    num_roots_(1), cells_(r * r * r + 1, 0) {
  // 0 is Super node at ground.
  fulled_[0] = true;
  cells_[0] = -1;
}

void UFMatrix::Add(const Vec3& pos) {
  assert(pos.IsValid(r_));
  int index = ToIndex(pos, r_);
  assert(!fulled_[index]);
  fulled_[index] = true;

  if (cells_[index] || dirty_) {
    // The node is once deleted. So reconstruct the whole UFSet.
    dirty_ = true;
    return;
  }

  // Create a new node.
  cells_[index] = -1;
  num_roots_++;

  // If this is at y == 0 plane, it is grounded.
  if (pos.y == 0) {
    Merge(0, index);
  }

  // Merge with neighbor cells.
  for (const auto& dp : neighbors) {
    Vec3 next = pos + dp;
    if (!next.IsValid(r_)) continue;
    Merge(index, ToIndex(next, r_));
  }
}

void UFMatrix::Remove(const Vec3& pos) {
  assert(pos.IsValid(r_));
  int index = ToIndex(pos, r_);
  assert(fulled_[index]);

  if (kIsDebug) {
    std::cerr << "Removing " << pos << "\n";
    Dump(std::cerr);
  }

  fulled_[index] = false;
  if (dirty_) {
    // The UFSet is already invalid.
    return;
  }

  // If the cell is isolated, just delete the cell.
  if (cells_[Find(index)] == -1) {
    if (kIsDebug)
      std::cerr << "Delete floating isolated node." << std::endl;
    // Note: leave cells_[root] == -1, as the marker of that the root node
    // is once used.
    --num_roots_;
    return;
  }

  if (AreNeighborCellsConnected(fulled_, r_, pos)) {
    if (kIsDebug) {
      std::cerr << "All neighbors are connected." << std::endl;
    }
    // All neighbor cells are connected each other even if this cell is
    // removed. Just update the number of alived cells.
    ++cells_[Find(index)];
    return;
  }

  if (kIsDebug)
    std::cerr << "Removing a cell splits tree. Reconstruct." << std::endl;
  dirty_ = true;
  return;
}

bool UFMatrix::IsGrounded(const Vec3& pos) {
  if (dirty_)
    Reconstruct();

  int index = ToIndex(pos, r_);
  return fulled_[index] && Find(index) == Find(0);
}

bool UFMatrix::IsAdjacentToGrounded(const Vec3& pos) {
  if (pos.y == 0)
    return true;

  for (const auto& dp : neighbors) {
    Vec3 next = pos + dp;
    if (!next.IsValid(r_)) continue;
    if (IsGrounded(next))
      return true;
  }
  return false;
}

bool UFMatrix::IsFull(const Vec3& pos) const {
  return fulled_[ToIndex(pos, r_)];
}

void UFMatrix::Dump(std::ostream& os) {
  if (dirty_) {
    os << "Dirty";
    return;
  }
  for (int x = 0; x < r_; ++x) {
    for (int y = 0; y < r_; ++y) {
      for (int z = 0; z < r_; ++z) {
        Vec3 pos{x, y, z};
        if (!IsFull(pos))
          continue;
        int index = ToIndex(pos, r_);
        int root = Find(index);
        os << pos << ", " << (!IsGrounded(pos) ? "not " : "")
           << "grounded, " << root << ", " << cells_[root]
           << ", " << cells_[index] << "\n";
      }
    }
  }
}

int UFMatrix::Find(int index) {
  int parent = cells_[index];

  // Unused node.
  if (parent == 0)
    return -1;

  // Root node.
  if (parent < 0)
    return index;

  int root = Find(parent - 1);
  assert(root >= 0);
  // Re-root.
  cells_[index] = root + 1;
  return root;
}

int UFMatrix::Find(int index) const {
  int parent = cells_[index];

  // Unused node.
  if (parent == 0)
    return -1;

  // Root node.
  if (parent < 0)
    return index;

  return Find(parent - 1);
}

void UFMatrix::Merge(int index1, int index2) {
  int root1 = Find(index1);
  int root2 = Find(index2);

  // If either is unused. Do nothing.
  if (root1 == -1 || root2 == -1)
    return;

  // If both are in the same tree, do nothing.
  if (root1 == root2)
    return;

  // Actual merge.
  if (root1 != 0 && cells_[root1] > cells_[root2])
    std::swap(root1, root2);
  cells_[root1] += cells_[root2];
  cells_[root2] = root1 + 1;
  num_roots_--;
}

void UFMatrix::Reconstruct() {
  dirty_ = false;

  // Reset all cells.
  num_roots_ = 0;
  for (int i = 0; i < fulled_.size(); ++i) {
    if (fulled_[i]) {
      cells_[i] = -1;
      ++num_roots_;
    } else {
      cells_[i] = 0;
    }
  }

  // Merge again.
  for (int i = 1; i < cells_.size(); ++i) {
    if (cells_[i] == 0)
      continue;
    auto p = ToVec(i, r_);
    if (p.y == 0)
      Merge(0, i);
    constexpr Vec3 kNeighborPositive[] = {
      {1, 0, 0}, {0, 1, 0}, {0, 0, 1}
    };
    for (const auto& dp : kNeighborPositive) {
      Vec3 next = p + dp;
      if (!next.IsValid(r_)) continue;
      Merge(i, ToIndex(next, r_));
    }
  }
}
