#ifndef UFMATRIX_H_
#define UFMATRIX_H_

#include <vector>
#include <ostream>

#include "vec3.h"

class UFMatrix {
 public:
  explicit UFMatrix(int r);

  void Add(const Vec3& pos);
  void Remove(const Vec3& pos);  // Inefficient.
  bool IsGrounded() {
    if (dirty_) Reconstruct();
    return num_roots_ == 1;
  }
  bool IsGrounded(const Vec3& pos);
  bool IsAdjacentToGrounded(const Vec3& pos);
  bool IsFull(const Vec3& pos) const;

  void Dump(std::ostream& os);

 private:
  int Find(int index);
  int Find(int index) const;
  void Merge(int index1, int index2);

  // Reconstruct UFSet from fulled_.
  void Reconstruct();

  int r_;
  // bit vector if the cell is filled or not.
  std::vector<bool> fulled_;

  int num_roots_;
  // 0: unused. positive: parent + 1. negative: is a root node. The tree has
  // |value| nodes.
  std::vector<int> cells_;
  bool dirty_ = false;
};

#endif  // UFMATRIX_H_
