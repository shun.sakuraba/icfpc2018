#include "default_solver.h"

#include "command_util.h"
#include "ufmatrix.h"
#include "model_util.h"

DefaultSolver::DefaultSolver(const Model* model) : model_(model) {}

std::vector<Command> DefaultSolver::Solve() {
  std::vector<Command> result;

  // Turn harmonics to High.
  bool is_low = true;
  UFMatrix ufmatrix(model_->r());

  int min_x = model_->min_x();
  int max_x = model_->max_x() + 1;
  int min_y = model_->min_y();
  int max_y = model_->max_y() + 1;
  int min_z = model_->min_z();
  int max_z = model_->max_z() + 1;

  int x = 0, y = 0, z = 0;
  int r = model_->r();

  for (int y0 = min_y; y0 < max_y; ++y0) {
    if (!model_util::HasFilledPixelInPlane(
            *model_, y0, min_z, max_z, min_x, max_x)) {
      continue;
    }

    command_util::MoveY(y0 + 1 - y, &result);
    y = y0 + 1;

    int z_min, z_max;
    std::tie(z_min, z_max) = model_util::FindMinMaxInPlane(
        *model_, y0, min_z, max_z, min_x, max_x);
    int dz, z_begin;
    if (std::abs(z_min - z) > std::abs(z_max - z)) {
      dz = -1;
      z_begin = z_max;
    } else {
      dz = 1;
      z_begin = z_min;
    }

    for (int z0 = z_begin; z_min <= z0 && z0 <= z_max; z0 += dz) {
      if (!model_util::HasFilledPixelInLine(*model_, y0, z0, min_x, max_x))
        continue;

      command_util::MoveZ(z0 - z, &result);
      z = z0;

      int x_min, x_max;
      std::tie(x_min, x_max) =
          model_util::FindMinMaxInLine(*model_, y0, z0, min_x, max_x);
      int dx, x_begin;
      if (std::abs(x_min - x) > std::abs(x_max - x)) {
        dx = -1;
        x_begin = x_max;
      } else {
        dx = 1;
        x_begin = x_min;
      }

      for (int x0 = x_begin; x_min <= x0 && x0 <= x_max; x0 += dx) {
        if (!model_->filled(Vec3{x0, y0, z0}))
          continue;
        command_util::MoveX(x0 - x, &result);
        x = x0;
        ufmatrix.Add(Vec3{x0, y0, z0});
        if (is_low && !ufmatrix.IsGrounded()) {
          result.emplace_back(Command::Flip());
          is_low = false;
        }

        result.emplace_back(Command::Fill(0, -1, 0));

        if (!is_low && ufmatrix.IsGrounded()) {
          result.emplace_back(Command::Flip());
          is_low = true;
        }
      }
    }
  }

  // Bot is at (x, y, z).
  command_util::MoveX(-x, &result);
  command_util::MoveZ(-z, &result);
  command_util::MoveY(-y, &result);

  result.emplace_back(Command::Halt());

  return result;
}
