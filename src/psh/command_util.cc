#include "command_util.h"

namespace command_util {

void MoveX(int dx, std::vector<Command>* trace) {
  if (dx == 0)
    return;

  if (dx < 0) {
    for (; dx < 0; dx += 15)
      trace->emplace_back(Command::SMove(-std::min(-dx, 15), 0, 0));
  } else {
    for (; dx > 0; dx -= 15)
      trace->emplace_back(Command::SMove(std::min(dx, 15), 0, 0));
  }
}

void MoveY(int dy, std::vector<Command>* trace) {
  if (dy == 0)
    return;

  if (dy < 0) {
    for (; dy < 0; dy += 15)
      trace->emplace_back(Command::SMove(0, -std::min(-dy, 15), 0));
  } else {
    for (; dy > 0; dy -= 15)
      trace->emplace_back(Command::SMove(0, std::min(dy, 15), 0));
  }
}

void MoveZ(int dz, std::vector<Command>* trace) {
  if (dz == 0)
    return;

  if (dz < 0) {
    for (; dz < 0; dz += 15)
      trace->emplace_back(Command::SMove(0, 0, -std::min(-dz, 15)));
  } else {
    for (; dz > 0; dz -= 15)
      trace->emplace_back(Command::SMove(0, 0, std::min(dz, 15)));
  }
}

void Merge(const std::vector<std::vector<Command>>& input,
           std::vector<Command>* output) {
  int max_size = 0;
  for (const auto& v : input) {
    max_size = std::max(max_size, static_cast<int>(v.size()));
  }

  for (int i = 0; i < max_size; ++i) {
    for (const auto& v : input) {
      if (v.size() <= i) {
        output->push_back(Command::Wait());
      } else {
        output->push_back(v[i]);
      }
    }
  }
}

}  // namespace command_util
