import argparse
import subprocess


def _submit(dry_run, private_token, drive_id, zipfile_path):
    sha256 = subprocess.check_output(
        ['sha256sum', zipfile_path]).decode('ascii').split()[0].strip()
    drive_url = 'https://drive.google.com/uc?id=' + drive_id
    command = [
        'curl', '-L',
        '--data-urlencode', 'action=submit',
        '--data-urlencode', 'privateID=' + private_token,
        '--data-urlencode', 'submissionURL=' + drive_url,
        '--data-urlencode', 'submissionSHA=' + sha256,
        'https://script.google.com/macros/s/AKfycbzQ7Etsj7NXCN5thGthCvApancl5vni5SFsb1UoKgZQwTzXlrH7/exec']
    print(command)
    if not dry_run:
        subprocess.check_call(command)


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dry-run', action='store_true')
    parser.add_argument('--private_token')
    parser.add_argument('--drive_id')
    parser.add_argument('--zip')
    return parser.parse_args()


def main():
    args = _parse_args()
    _submit(args.dry_run, args.private_token, args.drive_id, args.zip)

    
if __name__ == '__main__':
    main()
