#include "command.h"

#include <cassert>
#include <fstream>
#include <iostream>

namespace {

constexpr std::uint8_t kAxisX = 1;
constexpr std::uint8_t kAxisY = 2;
constexpr std::uint8_t kAxisZ = 3;

std::tuple<std::uint8_t, std::uint8_t> EncodeSld(const Vec3& sld) {
  if (sld.x != 0)
    return std::make_tuple(kAxisX, sld.x + 5);
  if (sld.y != 0)
    return std::make_tuple(kAxisY, sld.y + 5);
  if (sld.z != 0)
    return std::make_tuple(kAxisZ, sld.z + 5);

  // Error.
  return std::make_tuple(0, 0);
}

std::tuple<std::uint8_t, std::uint8_t> EncodeLld(const Vec3& lld) {
  if (lld.x != 0)
    return std::make_tuple(kAxisX, lld.x + 15);
  if (lld.y != 0)
    return std::make_tuple(kAxisY, lld.y + 15);
  if (lld.z != 0)
    return std::make_tuple(kAxisZ, lld.z + 15);

  // Error.
  return std::make_tuple(0, 0);
}

std::uint8_t EncodeNd(const Vec3&nd) {
  return (nd.x + 1) * 9 + (nd.y + 1) * 3 + (nd.z + 1);
}

Vec3 DecodeNd(std::uint8_t data) {
  int z = (data % 3) - 1;
  int y = (data / 3) % 3 - 1;
  int x = (data / 9) - 1;
  return {x, y, z};
}

std::uint32_t EncodeFd(const Vec3& fd) {
  return (fd.x + 30) | ((fd.y + 30) << 8) | ((fd.z + 30) << 16);
}

}  // namespace

std::tuple<std::uint32_t, int> Command::Encode() const {
  switch (type_) {
    case Type::Halt:
      return std::make_tuple(0xFF, 1);
    case Type::Wait:
      return std::make_tuple(0xFE, 1);
    case Type::Flip:
      return std::make_tuple(0xFD, 1);
    case Type::SMove: {
      auto lld = EncodeLld(arg1_);
      std::uint8_t lbyte = (std::get<0>(lld) << 4) | 0x4;
      std::uint8_t hbyte = std::get<1>(lld);
      return std::make_tuple((hbyte << 8) | lbyte, 2);
    }
    case Type::LMove: {
      auto sld1 = EncodeSld(arg1_);
      auto sld2 = EncodeSld(arg2_);
      std::uint8_t lbyte =
          (std::get<0>(sld2) << 6) |
          (std::get<0>(sld1) << 4) |
          0xC;
      std::uint8_t hbyte =
          (std::get<1>(sld2) << 4) |
          (std::get<1>(sld1));
      return std::make_tuple((hbyte << 8) | lbyte, 2);
    }
    case Type::FusionP: {
      auto nd = EncodeNd(arg1_);
      return std::make_tuple((nd << 3) | 0x7, 1);
    }
    case Type::FusionS: {
      auto nd = EncodeNd(arg1_);
      return std::make_tuple((nd << 3) | 0x6, 1);
    }
    case Type::Fission: {
      auto nd = EncodeNd(arg1_);
      auto m = arg2_.x;
      return std::make_tuple((nd << 3) | 0x5 | (m << 8), 2);
    }
    case Type::Fill: {
      auto nd = EncodeNd(arg1_);
      return std::make_tuple((nd << 3) | 0x3, 1);
    }
    case Type::Void: {
      auto nd = EncodeNd(arg1_);
      return std::make_tuple((nd << 3) | 0x2, 1);
    }
    case Type::GFill: {
      auto nd = EncodeNd(arg1_);
      auto fd = EncodeFd(arg2_);
      std::uint8_t lbyte = (nd << 3) | 0x1;
      return std::make_tuple((fd << 8) | lbyte, 4);
    }
    case Type::GVoid: {
      auto nd = EncodeNd(arg1_);
      auto fd = EncodeFd(arg2_);
      std::uint8_t lbyte = (nd << 3) | 0x0;
      return std::make_tuple((fd << 8) | lbyte, 4);
    }
  }
  return std::make_tuple(0, 0);
}

Command Command::Halt() {
  Command command;
  command.type_ = Type::Halt;
  return command;
}

Command Command::Wait() {
  Command command;
  command.type_ = Type::Wait;
  return command;
}

Command Command::Flip() {
  Command command;
  command.type_ = Type::Flip;
  return command;
}

Command Command::SMove(int x, int y, int z) {
  Command command;
  command.type_ = Type::SMove;
  command.arg1_ = Vec3{x, y, z};
  return command;
}

Command Command::LMove(int x1, int y1, int z1, int x2, int y2, int z2) {
  Command command;
  command.type_ = Type::LMove;
  command.arg1_ = Vec3{x1, y1, z1};
  command.arg2_ = Vec3{x2, y2, z2};
  return command;
}

Command Command::FusionP(int x, int y, int z) {
  Command command;
  command.type_ = Type::FusionP;
  command.arg1_ = Vec3{x, y, z};
  return command;
}

Command Command::FusionS(int x, int y, int z) {
  Command command;
  command.type_ = Type::FusionS;
  command.arg1_ = Vec3{x, y, z};
  return command;
}

Command Command::Fission(int x, int y, int z, std::uint8_t m) {
  Command command;
  command.type_ = Type::Fission;
  command.arg1_ = Vec3{x, y, z};
  command.arg2_.x = m;
  return command;
}

Command Command::Fill(int x, int y, int z) {
  Command command;
  command.type_ = Type::Fill;
  command.arg1_ = Vec3{x, y, z};
  return command;
}

Command Command::Void(int x, int y, int z) {
  Command command;
  command.type_ = Type::Void;
  command.arg1_ = Vec3{x, y, z};
  return command;
}

Command Command::GFill(int x1, int y1, int z1, int x2, int y2, int z2) {
  Command command;
  command.type_ = Type::GFill;
  command.arg1_ = Vec3{x1, y1, z1};
  command.arg2_ = Vec3{x2, y2, z2};
  return command;
}

Command Command::GVoid(int x1, int y1, int z1, int x2, int y2, int z2) {
  Command command;
  command.type_ = Type::GVoid;
  command.arg1_ = Vec3{x1, y1, z1};
  command.arg2_ = Vec3{x2, y2, z2};
  return command;
}

std::ostream& operator<<(std::ostream& os, const Command& command) {
  switch (command.type()) {
    case Command::Type::Halt:
      return os << "Halt";
    case Command::Type::Wait:
      return os << "Wait";
    case Command::Type::Flip:
      return os << "Flip";
    case Command::Type::SMove:
      return os << "SMove" << command.arg1();
    case Command::Type::LMove:
      return os << "LMove(" << command.arg1() << ", " << command.arg2() << ")";
    case Command::Type::FusionP:
      return os << "FusionP" << command.arg1();
    case Command::Type::FusionS:
      return os << "FusionS" << command.arg1();
    case Command::Type::Fission:
      return os << "Fission(" << command.arg1() << ", " << command.m() << ")";
    case Command::Type::Fill:
      return os << "Fill" << command.arg1();
    case Command::Type::Void:
      return os << "Void" << command.arg1();
    case Command::Type::GFill:
      return os << "GFill(" << command.arg1() << ", " << command.arg2() << ")";
    case Command::Type::GVoid:
      return os << "GVoid(" << command.arg1() << ", " << command.arg2() << ")";
  }
  // Unknown command.
  return os;
}

bool OutputTrace(const std::vector<Command>& trace,
                 const std::string& path) {
  std::ofstream stream(path, std::ios_base::binary);
  if (!stream) {
    std::cerr << "Failed to open: " << path << std::endl;
    return false;
  }

  for (const auto& command : trace) {
    auto encoded = command.Encode();
    auto data = std::get<0>(encoded);
    auto len = std::get<1>(encoded);
    for (int i = 0; i < len; ++i) {
      stream.put(data & 0xFF);
      data >>= 8;
    }
  }

  return true;
}

std::vector<Command> ParseTrace(const std::string& path) {
  std::ifstream stream(path, std::ios_base::binary);
  if (!stream) {
    std::cerr << "Failed to open: " << path << std::endl;
    return {};
  }

  std::vector<Command> result;
  while (true) {
    std::uint8_t data = static_cast<std::uint8_t>(stream.get());
    if (!stream)
      break;

    if (data == 0xFF) {
      result.push_back(Command::Halt());
      continue;
    }
    if (data == 0xFE) {
      result.push_back(Command::Wait());
      continue;
    }
    if (data == 0xFD) {
      result.push_back(Command::Flip());
      continue;
    }
    if ((data & 0xF) == 0x4) {
      // SMove.
      int d = stream.get() - 15;
      switch (data >> 4) {
        case kAxisX:
          result.push_back(Command::SMove(d, 0, 0));
          break;
        case kAxisY:
          result.push_back(Command::SMove(0, d, 0));
          break;
        case kAxisZ:
          result.push_back(Command::SMove(0, 0, d));
          break;
        default:
          std::cerr << "Unknoen axis." << std::endl;
          return {};
      }
      continue;
    }
    if ((data & 0xF) == 0xC) {
      // LMove.
      std::uint8_t data2 = static_cast<std::uint8_t>(stream.get());
      int d1 = (data2 & 0xF) - 5;
      Vec3 vd1;
      switch ((data >> 4) & 0x3) {
        case kAxisX: vd1 = Vec3{d1, 0, 0}; break;
        case kAxisY: vd1 = Vec3{0, d1, 0}; break;
        case kAxisZ: vd1 = Vec3{0, 0, d1}; break;
      }
      int d2 = (data2 >> 4) - 5;
      Vec3 vd2;
      switch ((data >> 6) & 0x3) {
        case kAxisX: vd2 = Vec3{d2, 0, 0}; break;
        case kAxisY: vd2 = Vec3{0, d2, 0}; break;
        case kAxisZ: vd2 = Vec3{0, 0, d2}; break;
      }
      result.push_back(
          Command::LMove(vd1.x, vd1.y, vd1.z, vd2.x, vd2.y, vd2.z));
      continue;
    }
    if ((data & 0x7) == 0x7) {
      // Fusion P.
      Vec3 nd = DecodeNd(data >> 3);
      result.push_back(Command::FusionP(nd.x, nd.y, nd.z));
      continue;
    }
    if ((data & 0x7) == 0x6) {
      // Fusion S.
      Vec3 nd = DecodeNd(data >> 3);
      result.push_back(Command::FusionS(nd.x, nd.y, nd.z));
      continue;
    }
    if ((data & 0x7) == 0x5) {
      // Fission.
      Vec3 nd = DecodeNd(data >> 3);
      std::uint8_t m = static_cast<std::uint8_t>(stream.get());
      result.push_back(Command::Fission(nd.x, nd.y, nd.z, m));
      continue;
    }
    if ((data & 0x7) == 0x3) {
      // Fill.
      Vec3 nd = DecodeNd(data >> 3);
      result.push_back(Command::Fill(nd.x, nd.y, nd.z));
      continue;
    }
    if ((data & 0x7) == 0x2) {
      // Void.
      Vec3 nd = DecodeNd(data >> 3);
      result.push_back(Command::Void(nd.x, nd.y, nd.z));
      continue;
    }
    if ((data & 0x7) == 0x1) {
      // GFill.
      Vec3 nd = DecodeNd(data >> 3);
      int x = stream.get() - 30;
      int y = stream.get() - 30;
      int z = stream.get() - 30;
      result.push_back(Command::GFill(nd.x, nd.y, nd.z, x, y, z));
      continue;
    }
    if ((data & 0x7) == 0x0) {
      // GFill.
      Vec3 nd = DecodeNd(data >> 3);
      int x = stream.get() - 30;
      int y = stream.get() - 30;
      int z = stream.get() - 30;
      result.push_back(Command::GVoid(nd.x, nd.y, nd.z, x, y, z));
      continue;
    }

    std::cerr << "Unknown command: " << std::hex << (int)data << std::endl;
    return {};
  }

  return result;
}
