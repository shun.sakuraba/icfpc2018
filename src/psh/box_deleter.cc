#include "box_deleter.h"

#include "command_util.h"

BoxDeleter::BoxDeleter(const Model* model) : model_(model) {}


std::vector<Command> BoxDeleter::Solve() {
  int min_x = model_->min_x();
  int max_x = model_->max_x();
  int min_y = model_->min_y();
  int max_y = model_->max_y();
  int min_z = model_->min_z();
  int max_z = model_->max_z();

  int width = model_->max_x() - model_->min_x() + 1;
  int height = model_->max_y() - model_->min_y() + 1;
  int depth = model_->max_z() - model_->min_z() + 1;

  // Give up.
  if (width > 30 || height > 30 || depth > 30)
    return {};

  std::vector<Command> result;

  // Move to box origin.
  command_util::MoveX(min_x - 1, &result);
  command_util::MoveZ(min_z - 1, &result);

  // Phase Fission.
  result.push_back(Command::Fission(1, 0, 0, 3));
  {
    std::vector<Command> tmp;
    command_util::MoveX((max_x + 1) - (min_x - 1) - 1, &tmp);
    command_util::Merge({std::vector<Command>(), std::move(tmp)}, &result);
  }

  result.push_back(Command::Fission(0, 0, 1, 1));
  result.push_back(Command::Fission(0, 0, 1, 1));
  {
    std::vector<Command> tmp;
    command_util::MoveZ((max_z + 1) - (min_z - 1) - 1, &tmp);
    command_util::Merge(
        {std::vector<Command>(), std::vector<Command>(), tmp, tmp}, &result);
  }

  result.push_back(Command::Fission(0, 1, 0, 0));
  result.push_back(Command::Fission(0, 1, 0, 0));
  result.push_back(Command::Fission(0, 1, 0, 0));
  result.push_back(Command::Fission(0, 1, 0, 0));
  {
    std::vector<Command> tmp;
    command_util::MoveY(max_y - min_y - 1, &tmp);
    command_util::Merge(
        {std::vector<Command>(), std::vector<Command>(),
         std::vector<Command>(), tmp, tmp,
         std::vector<Command>(), tmp, tmp},
        &result);
  }

  // Fire GVoid.
  result.push_back(
      Command::GVoid(1, 0, 1, width - 1, height - 1, depth - 1));
  result.push_back(
      Command::GVoid(-1, 0, 1, -(width - 1), height - 1, depth - 1));
  result.push_back(
      Command::GVoid(-1, 0, -1, -(width - 1), height - 1, -(depth - 1)));
  result.push_back(
      Command::GVoid(-1, 0, -1, -(width - 1), -(height - 1), -(depth - 1)));
  result.push_back(
      Command::GVoid(-1, 0, 1, -(width - 1), -(height - 1), depth - 1));
  result.push_back(
      Command::GVoid(1, 0, -1, width - 1, height - 1, -(depth - 1)));
  result.push_back(
      Command::GVoid(1, 0, -1, width - 1, -(height - 1), -(depth - 1)));
  result.push_back(
      Command::GVoid(1, 0, 1, width - 1, -(height - 1), depth - 1));

  // Phase Fusion.
  {
    std::vector<Command> tmp;
    command_util::MoveY(-(max_y - min_y - 1), &tmp);
    command_util::Merge(
        {std::vector<Command>(), std::vector<Command>(),
         std::vector<Command>(), tmp, tmp,
         std::vector<Command>(), tmp, tmp},
        &result);
  }
  result.push_back(Command::FusionP(0, 1, 0));
  result.push_back(Command::FusionP(0, 1, 0));
  result.push_back(Command::FusionP(0, 1, 0));
  result.push_back(Command::FusionS(0, -1, 0));
  result.push_back(Command::FusionS(0, -1, 0));
  result.push_back(Command::FusionP(0, 1, 0));
  result.push_back(Command::FusionS(0, -1, 0));
  result.push_back(Command::FusionS(0, -1, 0));

  {
    std::vector<Command> tmp;
    command_util::MoveZ(-((max_z + 1) - (min_z - 1) - 1), &tmp);
    command_util::Merge(
        {std::vector<Command>(), std::vector<Command>(), tmp, tmp}, &result);
  }
  result.push_back(Command::FusionP(0, 0, 1));
  result.push_back(Command::FusionP(0, 0, 1));
  result.push_back(Command::FusionS(0, 0, -1));
  result.push_back(Command::FusionS(0, 0, -1));

  {
    std::vector<Command> tmp;
    command_util::MoveX(-((max_x + 1) - (min_x - 1) - 1), &tmp);
    command_util::Merge({std::vector<Command>(), std::move(tmp)}, &result);
  }
  result.push_back(Command::FusionP(1, 0, 0));
  result.push_back(Command::FusionS(-1, 0, 0));

  // Move to origin.
  command_util::MoveX(-(min_x - 1), &result);
  command_util::MoveZ(-(min_z - 1), &result);
  result.push_back(Command::Halt());

  return result;
}

