#ifndef ARRAY_BY_VEC3_H_
#define ARRAY_BY_VEC3_H_

#include "vec3.h"
#include <vector>

// Vector indexed by Vec3.
template <typename T> class ArrayByVec3 final {
public:
  explicit ArrayByVec3(int r, const T &init = {})
      : r_(r), data_(r * r * r, init) {}
  T &operator[](const Vec3 &pos) {
    int index = pos.x * r_ * r_ + pos.y * r_ + pos.z;
    return data_[index];
  }

private:
  const int r_;
  std::vector<T> data_;
};

#endif
