#include "dc_solver.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include "command_util.h"
#include "model_util.h"
#include "state.h"

namespace {

struct Worker {
  int min_x, max_x;
  int min_z, max_z;

  Vec3 current;
};

std::tuple<std::vector<Command>, Vec3> SolveRange(
    const Model* model,
    const Vec3& init_pos,
    int min_x, int max_x, int min_y, int max_y, int min_z, int max_z) {
  std::vector<Command> result;
  int x = init_pos.x, y = init_pos.y, z = init_pos.z;

  for (int y0 = min_y; y0 < max_y; ++y0) {
    if (!model_util::HasFilledPixelInPlane(
            *model, y0, min_z, max_z, min_x, max_x)) {
      continue;
    }

    command_util::MoveY(y0 + 1 - y, &result);
    y = y0 + 1;

    int z_min, z_max;
    std::tie(z_min, z_max) = model_util::FindMinMaxInPlane(
        *model, y0, min_z, max_z, min_x, max_x);
    int dz, z_begin;
    if (std::abs(z_min - z) > std::abs(z_max - z)) {
      dz = -1;
      z_begin = z_max;
    } else {
      dz = 1;
      z_begin = z_min;
    }

    for (int z0 = z_begin; z_min <= z0 && z0 <= z_max; z0 += dz) {
      if (!model_util::HasFilledPixelInLine(*model, y0, z0, min_x, max_x))
        continue;

      command_util::MoveZ(z0 - z, &result);
      z = z0;

      int x_min, x_max;
      std::tie(x_min, x_max) =
          model_util::FindMinMaxInLine(*model, y0, z0, min_x, max_x);
      int dx, x_begin;
      if (std::abs(x_min - x) > std::abs(x_max - x)) {
        dx = -1;
        x_begin = x_max;
      } else {
        dx = 1;
        x_begin = x_min;
      }

      for (int x0 = x_begin; x_min <= x0 && x0 <= x_max; x0 += dx) {
        if (!model->filled(Vec3{x0, y0, z0}))
          continue;
        command_util::MoveX(x0 - x, &result);
        x = x0;
        result.emplace_back(Command::Fill(0, -1, 0));
      }
    }
  }

  command_util::MoveX(init_pos.x - x, &result);
  command_util::MoveZ(init_pos.z - z, &result);

  return std::make_tuple(std::move(result), Vec3{init_pos.x, y, init_pos.z});
}

std::vector<Command> InsertFlip(const Model* model,
                                const std::vector<Command>& trace) {
  State state(nullptr, model, std::vector<Command>(trace));
  std::vector<Command> result;
  bool is_low = true;
  while (!state.IsFinished()) {
    if (!is_low && state.matrix().IsGrounded()) {
      int n = state.bots().size();
      result.push_back(Command::Flip());
      for (int i = 1; i < n; ++i) {
        result.push_back(Command::Wait());
      }
      is_low = true;
      state.Flip();
    }

    int old_trace = state.current_trace();
    if (!state.ExecuteStep()) {
      std::cerr << "Failed to move next step." << std::endl;
      return {};
    }
    if (is_low && !state.matrix().IsGrounded()) {
      int n = state.bots().size();
      result.push_back(Command::Flip());
      for (int i = 1; i < n; ++i) {
        result.push_back(Command::Wait());
      }
      is_low = false;
      state.Flip();
    }
    int new_trace = state.current_trace();
    result.insert(
        result.end(), trace.begin() + old_trace, trace.begin() + new_trace);
  }

  return result;
}

}  // namespace

DCSolver::DCSolver(const Model* model) : model_(model) {}

// +--+--+--+--+
// |19|15|11| 7|
// +--+--+--+--+ z4
// |18|14|10| 6|
// +--+--+--+--+ z3
// |17|13| 9| 5|
// +--+--+--+--+ z2
// |16|12| 8| 4|
// +--+--+--+--+ z1
// | 0| 1| 2| 3|
// +--+--+--+--+ z0
// x0 x1 x2 x3
std::vector<Command> DCSolver::Solve() {
  constexpr int num_x = 4;
  constexpr int num_z = 5;

  int min_r = 5;
  int model_width = model_->max_x() - model_->min_x() + 1;
  int model_depth = model_->max_z() - model_->min_z() + 1;
  if (model_width < min_r || model_depth < min_r) {
    // Too small. Just give up.
    return {};
  }

  int width = model_width / num_x;
  int xs[num_x + 1];
  xs[0] = model_->min_x();
  for (int i = 1; i <= num_x; ++i) {
    xs[i] = xs[i - 1] + width + (i <= model_width % num_x ? 1 : 0);
  }

  int depth = model_depth / num_z;
  int zs[num_z + 1];
  zs[0] = model_->min_z();
  for (int i = 1; i <= num_z; ++i) {
    zs[i] = zs[i - 1] + depth + (i <= model_depth % num_z ? 1 : 0);
  }

  std::vector<Worker> workers;
  workers.push_back(Worker{xs[0], xs[1], zs[0], zs[1]});
  workers.push_back(Worker{xs[1], xs[2], zs[0], zs[1]});
  workers.push_back(Worker{xs[2], xs[3], zs[0], zs[1]});
  workers.push_back(Worker{xs[3], xs[4], zs[0], zs[1]});

  workers.push_back(Worker{xs[3], xs[4], zs[1], zs[2]});
  workers.push_back(Worker{xs[3], xs[4], zs[2], zs[3]});
  workers.push_back(Worker{xs[3], xs[4], zs[3], zs[4]});
  workers.push_back(Worker{xs[3], xs[4], zs[4], zs[5]});

  workers.push_back(Worker{xs[2], xs[3], zs[1], zs[2]});
  workers.push_back(Worker{xs[2], xs[3], zs[2], zs[3]});
  workers.push_back(Worker{xs[2], xs[3], zs[3], zs[4]});
  workers.push_back(Worker{xs[2], xs[3], zs[4], zs[5]});

  workers.push_back(Worker{xs[1], xs[2], zs[1], zs[2]});
  workers.push_back(Worker{xs[1], xs[2], zs[2], zs[3]});
  workers.push_back(Worker{xs[1], xs[2], zs[3], zs[4]});
  workers.push_back(Worker{xs[1], xs[2], zs[4], zs[5]});

  workers.push_back(Worker{xs[0], xs[1], zs[1], zs[2]});
  workers.push_back(Worker{xs[0], xs[1], zs[2], zs[3]});
  workers.push_back(Worker{xs[0], xs[1], zs[3], zs[4]});
  workers.push_back(Worker{xs[0], xs[1], zs[4], zs[5]});

  std::vector<Command> result;

  // First of all, divide X.
  command_util::MoveX(xs[0], &result);
  command_util::MoveZ(zs[0], &result);

  result.push_back(Command::Fission(1, 0, 0, 14));
  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[1] - xs[0] - 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }

  result.push_back(Command::Wait());
  result.push_back(Command::Fission(1, 0, 0, 9));
  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[2] - xs[1] - 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }

  result.push_back(Command::Wait());
  result.push_back(Command::Wait());
  result.push_back(Command::Fission(1, 0, 0, 4));
  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[3] - xs[2] - 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }

  // Then divide Z.
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Fission(0, 0, 1, 3));
  }

  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[1] - zs[0] - 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(c);
      }
    }
  }

  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Fission(0, 0, 1, 2));
  }
  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[2] - zs[1] - 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }

  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
    result.push_back(Command::Fission(0, 0, 1, 1));
  }
  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[3] - zs[2] - 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }

  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
    result.push_back(Command::Wait());
    result.push_back(Command::Fission(0, 0, 1, 0));
  }
  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[4] - zs[3] - 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }

#if 0
  // TODO: use harmonics low.
  result.push_back(Command::Flip());
  for (int i = 1; i < num_x * num_z; ++i) {
    result.push_back(Command::Wait());
  }
#endif

  // TODO solve each reasion.
  {
    std::vector<std::vector<Command>> range_result;
    for (auto& worker : workers) {
      auto worker_result = SolveRange(
          model_,
          Vec3{worker.min_x, 0, worker.min_z},
          worker.min_x, worker.max_x, model_->min_y(), model_->max_y() + 1,
          worker.min_z, worker.max_z);
      worker.current = std::get<1>(worker_result);
      range_result.push_back(std::get<0>(std::move(worker_result)));
    }

    command_util::Merge(range_result, &result);
  }

#if 0
  // TODO:
  result.push_back(Command::Flip());
  for (int i = 1; i < workers.size(); ++i) {
    result.push_back(Command::Wait());
  }
#endif

  // Move to y-max.
  {
    std::vector<std::vector<Command>> ymove;
    for (const auto& worker : workers) {
      std::vector<Command> tmp;
      command_util::MoveY(model_->max_y() + 1 - worker.current.y, &tmp);
      ymove.push_back(std::move(tmp));
    }

    command_util::Merge(ymove, &result);
  }

  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[3] - zs[4] + 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
    result.push_back(Command::Wait());
    result.push_back(Command::FusionP(0, 0, 1));
    result.push_back(Command::FusionS(0, 0, -1));
  }

  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[2] - zs[3] + 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
    result.push_back(Command::FusionP(0, 0, 1));
    result.push_back(Command::FusionS(0, 0, -1));
  }

  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[1] - zs[2] + 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
        result.push_back(c);
      }
    }
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::Wait());
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::FusionP(0, 0, 1));
    result.push_back(Command::FusionS(0, 0, -1));
  }

  {
    std::vector<Command> tmp;
    command_util::MoveZ(zs[0] - zs[1] + 1, &tmp);
    for (const auto& c : tmp) {
      for (int i = 0; i < num_x; ++i) {
        result.push_back(Command::Wait());
      }
      for (int i = 0; i < num_x; ++i) {
        result.push_back(c);
      }
    }
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::FusionP(0, 0, 1));
  }
  for (int i = 0; i < num_x; ++i) {
    result.push_back(Command::FusionS(0, 0, -1));
  }

  // Merge X.
  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[2] - xs[3] + 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }
  result.push_back(Command::Wait());
  result.push_back(Command::Wait());
  result.push_back(Command::FusionP(1, 0, 0));
  result.push_back(Command::FusionS(-1, 0, 0));

  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[1] - xs[2] + 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }
  result.push_back(Command::Wait());
  result.push_back(Command::FusionP(1, 0, 0));
  result.push_back(Command::FusionS(-1, 0, 0));

  {
    std::vector<Command> tmp;
    command_util::MoveX(xs[0] - xs[1] + 1, &tmp);
    for (const auto& c : tmp) {
      result.push_back(Command::Wait());
      result.push_back(c);
    }
  }
  result.push_back(Command::FusionP(1, 0, 0));
  result.push_back(Command::FusionS(-1, 0, 0));

  command_util::MoveX(-xs[0], &result);
  command_util::MoveZ(-zs[0], &result);
  command_util::MoveY(-(model_->max_y() + 1), &result);
  result.push_back(Command::Halt());

  return InsertFlip(model_, result);
}
