#ifndef BOX_DELETER_H_
#define BOX_DELETER_H_

#include "command.h"
#include "model.h"

#include <vector>

class BoxDeleter {
 public:
  explicit BoxDeleter(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;
};

#endif  // BOX_DELETER_H_
