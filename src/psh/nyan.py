import argparse
import datetime
import json
import multiprocessing
import os
import re
import subprocess
import sys
import zipfile

_DEFAULT_SOLVERS = {
    'assemble': {
        'default': ['./driver', 'default'],
        'dc': ['./driver', 'dc'],
        'dc2': ['./driver', 'dc2'],
        'ki1': ['./driver', 'ki1'],
        'ki8': ['./driver', 'ki8'],
        'ki20': ['./driver', 'ki20'],
        'mission': ['./driver', 'mission'],
    },
    'disassemble': {
        'default': ['./driver', 'default'],
        'dc': ['./driver', 'dc'],
        'dc2': ['./driver', 'dc2'],
        'ki1': ['./driver', 'ki1'],
        'ki8': ['./driver', 'ki8'],
        'ki20': ['./driver', 'ki20'],
        'mission': ['./driver', 'mission'],
        'box': ['./driver', 'box'],
    },
    'reassemble': {
        'default': ['./driver', 'default'],
        'dc': ['./driver', 'dc'],
        'dc2': ['./driver', 'dc2'],
        'ki1': ['./driver', 'ki1'],
        'ki8': ['./driver', 'ki8'],
        'ki20': ['./driver', 'ki20'],
        'mission': ['./driver', 'mission'],
    },
}

_DEFAULT_PROBLEMS = (
    ['FA%03d' % i for i in range(1, 187)] +
    ['FD%03d' % i for i in range(1, 187)] +
    ['FR%03d' % i for i in range(1, 116)])


def _run(solvers, problem_dir, problem, output_dir):
    print('Solving %s ...' % problem, file=sys.stderr)

    if problem[1] == 'A':
        mode = 'assemble'
    elif problem[1] == 'D':
        mode = 'disassemble'
    elif problem[1] == 'R':
        mode = 'reassemble'
    else:
        return problem, {}

    if solvers is None:
        solvers = _DEFAULT_SOLVERS[mode]

    result = {}
    for name, command in solvers.items():
        problem_files = []
        if mode in ('disassemble', 'reassemble'):
            problem_files.append(
                os.path.join(problem_dir, '%s_src.mdl' % problem))
        if mode in ('assemble', 'reassemble'):
            problem_files.append(
                os.path.join(problem_dir, '%s_tgt.mdl' % problem))

        output_file = os.path.join(output_dir, '%s_%s.nbt' % (problem, name))
        # TODO: proxy logs.
        subprocess_result = subprocess.run(
            command + [mode] + problem_files + [output_file],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if subprocess_result.returncode != 0:
            result[name] = (-1, output_file)
            continue
        subprocess_result = subprocess.run(
            ['./executor', mode] + problem_files + [output_file],
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        if subprocess_result.returncode != 0:
            result[name] = (-2, output_file)
            continue
        m = re.match(r'Energy: (\d+)',
                     subprocess_result.stdout.decode('utf-8'))
        result[name] = (int(m.group(1)), output_file)
        print("  %15d %s %s" % (int(m.group(1)), problem, name))
    return problem, result


def _create_archive(result, submit_zip):
    best_result = {}
    for problem, problem_result in result.items():
        best = None
        for score, path in problem_result.values():
            if score < 0:
                continue
            if best is None or best[0] > score:
                best = (score, path)
        best_result[problem] = best[1]

    with zipfile.ZipFile(submit_zip, 'w') as z:
        for problem, path in best_result.items():
            z.write(path, '%s.nbt' % problem)


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--solver', nargs='*',
                        help='Solver, in the "name=commandline" format.')
    parser.add_argument('--problem',
                        help='Comma separated problem number list.')
    parser.add_argument('--problem_dir',
                        help='Directory path to the problem mdl files.')
    parser.add_argument('--output_dir',
                        help='Directory path to contain the output data.')
    parser.add_argument('--submit_zip',
                        help='Path to the output submit ready zip file.')
    parser.add_argument('--j', type=int)
    # TODO create zip.
    return parser.parse_args()


def main():
    args = _parse_args()
    if args.solver:
        solvers = {item.split('=', 1) for item in args.solver}
    else:
        solvers = None
    if args.problem:
        problem_list = args.problem.split(',')
    else:
        problem_list = _DEFAULT_PROBLEMS
    if args.problem_dir:
        problem_dir = args.problem_dir
    else:
        problem_dir = os.path.join(
            os.path.dirname(__file__), '../../data/problemsF')
    if args.output_dir:
        output_dir = args.output_dir
    else:
        now = datetime.datetime.now()
        output_dir = now.strftime('/tmp/nyan.%Y%m%d-%H%M%S')
    os.makedirs(output_dir, exist_ok=True)

    if args.submit_zip:
        submit_zip = args.submit_zip
    else:
        submit_zip = output_dir + '.zip'

    print('Output: %s' % output_dir)
    result = {}
    if args.j is None:
        for problem in problem_list:
            key, value = _run(solvers, problem_dir, problem, output_dir)
            result[key] = value
    else:
        j = args.j or multiprocessing.cpu_count()
        with multiprocessing.Pool(processes=j) as p:
            for key, value in p.starmap(
                    _run, [(solvers, problem_dir, problem, output_dir)
                           for problem in problem_list]):
                result[key] = value

    s = json.dumps(result, indent=2)
    print(s)
    with open(os.path.join(output_dir, 'result.json'), 'w') as f:
        print(s, file=f)

    print('Createing %s...' % submit_zip)
    _create_archive(result, submit_zip)


if __name__ == '__main__':
    main()
