import os
import hashlib
import json
import datetime
import zipfile
import subprocess
import multiprocessing
import re

_ASSEMBLE_SOLVERS = {
    'default': ['./driver', 'default'],
    'dc': ['./driver', 'dc'],
    'dc2': ['./driver', 'dc2'],
    'ki1': ['./driver', 'ki1'],
    'ki8': ['./driver', 'ki8'],
    'ki20': ['./driver', 'ki20'],
#    'mission': ['./driver', 'mission'],
}

_DISASSEMBLE_SOLVERS = {
    'box': ['./driver', 'box'],
}

_ASSEMBLE_PROBLEMS = ['FA%03d' % i for i in range(1, 187)]
_DISASSEMBLE_PROBLEMS = ['FD%03d' % i for i in range(1, 187)]
_REASSEMBLE_PROBLEMS = ['FR%03d' % i for i in range(1, 116)]

_TIMEOUT = 90
_EVAL_TIMEOUT = 15

def _evaluate(mode, input_model, trace):
    try:
        result = subprocess.run(
            ['./executor', mode, input_model, trace],
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
            timeout=_EVAL_TIMEOUT)
    except subprocess.TimeoutExpired:
        return -4

    if result.returncode != 0:
        return -2

    m = re.match(r'Energy: (\d+)', result.stdout.decode('utf-8'))
    score = int(m.group(1))
    return score


def _run(sha256, mode, solver, input_file, output_dir):
    output_file = '%s_%s.nbt' % (sha256, solver[0])
    output_path = os.path.join(output_dir, output_file)
    try:
        result = subprocess.run(
            solver[1] + [mode, input_file, output_path],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
            timeout=_TIMEOUT)
    except subprocess.TimeoutExpired:
        return (sha256, solver[0], -3, None)

    if result.returncode != 0:
        return (sha256, solver[0], -1, None)

    score = _evaluate(mode, input_file, output_path)
    return (sha256, solver[0], score, output_path)

def _run_wrapper(job):
    return _run(*job)


def _reverse_trace(model_path, input_trace, output_trace):
    subprocess.check_call(
        ['./reverse_trace', model_path, input_trace, output_trace],
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def _find_best(result_list):
    best = None
    for name, score, path in result_list:
        if score < 0:
            continue
        if best is None or best[1] > score:
            best = (name, score, path)
    return best


def _create_disassemble_from_assemble(
        sha256, name, assemble_score, model_path, input_path, output_path):
    _reverse_trace(model_path, input_path, output_path)
    # Looks slow. Use assemble_score always.
    # score = _evaluate('disassemble', model_path, output_path)
    score = assemble_score

    return sha256, name, score, output_path


def _update_disassemble_map_internal(job):
    return _create_disassemble_from_assemble(*job)


def _update_disassemble_map(
        j, sha256_group_map, assemble_map, disassemble_map,
        input_dir, output_dir):
    jobs = []
    for sha256, file_group in sha256_group_map.items():
        if not (any(filename.endswith('_src.mdl') for filename in file_group)):
            continue

        best = _find_best(assemble_map[sha256])
        if best is None:
            continue

        model_path = os.path.join(input_dir, file_group[0])
        name = best[0] + '_reversed'
        input_path = best[2]
        output_file = '%s_%s.nbt' % (sha256, name)
        output_path = os.path.join(output_dir, output_file)
        jobs.append((
            sha256, name, best[1], model_path, input_path, output_path))

    with multiprocessing.Pool(processes=j) as p:
        for result in p.imap_unordered(
                _update_disassemble_map_internal, jobs):
            print(result)
            disassemble_map.setdefault(result[0], []).append(result[1:])


def _solve_assemble(problem, sha256_map, assemble_map, output_dir):
    sha256 = sha256_map[problem + '_tgt.mdl']
    return _find_best(assemble_map[sha256])


def _solve_disassemble(problem, sha256_map, disassemble_map, output_dir):
    sha256 = sha256_map[problem + '_src.mdl']
    return _find_best(disassemble_map[sha256])


def _solve_reassemble(problem, sha256_map, assemble_map, disassemble_map,
                      output_dir):
    source_sha256 = sha256_map[problem + '_src.mdl']
    target_sha256 = sha256_map[problem + '_tgt.mdl']
    source_best = _find_best(disassemble_map[source_sha256])
    target_best = _find_best(assemble_map[target_sha256])
    if not source_best or not target_best:
        print(problem)
    # Compose.
    output_path = os.path.join(output_dir, '%s_composed.ndt' % problem)
    with open(output_path, 'wb') as f:
        with open(source_best[2], 'rb') as f1:
            f.write(f1.read()[:-1])  # Drop last halt.
        with open(target_best[2], 'rb') as f2:
            f.write(f2.read())
    return ('composed', source_best[1] + target_best[1], output_path)


def _group_files(input_dir):
    result = {}
    for filename in os.listdir(input_dir):
        if not filename.endswith('.mdl'):
            continue
        path = os.path.join(input_dir, filename)
        h = hashlib.sha256()
        with open(path, 'rb') as f:
            h.update(f.read())
        sha256 = h.hexdigest()
        result.setdefault(sha256, []).append(filename)
    for value in result.values():
        value.sort()
    return result

def _create_archive(result, output_zip):
    with zipfile.ZipFile(output_zip, 'w',
                         compression=zipfile.ZIP_DEFLATED) as z:
        for problem, solution in result.items():
            z.write(solution[2], problem + '.nbt')


def main():
    input_dir = '../../data/problemsF'

    now = datetime.datetime.now()
    output_dir = now.strftime('/tmp/nyan2.%Y%m%d-%H%M%S')
    os.makedirs(output_dir, exist_ok=True)

    sha256_group_map = _group_files(input_dir)
    with open(os.path.join(output_dir, 'file_group.json'), 'w') as f:
        json.dump(sha256_group_map, f, indent=2)

    assemble_jobs = (
        [(sha256, 'assemble', (solver_name, solver_command),
          os.path.join(input_dir, model_files[0]), output_dir)
         for solver_name, solver_command in _ASSEMBLE_SOLVERS.items()
         for sha256, model_files in sha256_group_map.items()])
    disassemble_jobs = (
        [(sha256, 'disassemble', (solver_name, solver_command),
          os.path.join(input_dir, model_files[0]), output_dir)
         for solver_name, solver_command in _DISASSEMBLE_SOLVERS.items()
         for sha256, model_files in sha256_group_map.items()
         if any(filepath.endswith('_src.mdl') for filepath in model_files)])

    j = multiprocessing.cpu_count()
    with multiprocessing.Pool(processes=j) as p:
        assemble_map = {}
        for result in p.imap_unordered(_run_wrapper, assemble_jobs):
            print(result)
            assemble_map.setdefault(result[0], []).append(result[1:])
        disassemble_map = {}
        for result in p.imap_unordered(_run_wrapper, disassemble_jobs):
            print(result)
            disassemble_map.setdefault(result[0], []).append(result[1:])

    _update_disassemble_map(
        j, sha256_group_map, assemble_map, disassemble_map,
        input_dir, output_dir)

    with open(os.path.join(output_dir, 'assemble.json'), 'w') as f:
        json.dump(assemble_map, f, indent=2)
    with open(os.path.join(output_dir, 'disassemble.json'), 'w') as f:
        json.dump(disassemble_map, f, indent=2)

    sha256_map = {}
    for sha256, file_group in sha256_group_map.items():
        for filename in file_group:
            sha256_map[filename] = sha256

    result = {}
    for problem in _ASSEMBLE_PROBLEMS:
        result[problem] = _solve_assemble(
            problem, sha256_map, assemble_map, output_dir)
    for problem in _DISASSEMBLE_PROBLEMS:
        result[problem] = _solve_disassemble(
            problem, sha256_map, disassemble_map, output_dir)
    for problem in _REASSEMBLE_PROBLEMS:
        result[problem] = _solve_reassemble(
            problem, sha256_map, assemble_map, disassemble_map, output_dir)

    with open(os.path.join(output_dir, 'result.json'), 'w') as f:
        json.dump(result, f, indent=2)
    print(json.dumps(result, indent=2))

    print('Creating an archive...')
    archive_path = output_dir + '.zip'
    _create_archive(result, archive_path)


if __name__ == '__main__':
    main()
