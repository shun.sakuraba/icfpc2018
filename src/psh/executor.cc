#include "state.h"

#include <iostream>

namespace {

int ExecuteInternal(const Model* source_model,
                    const Model* target_model,
                    const std::string& trace_path) {
  std::cerr << "R: " << static_cast<int>(target_model->r()) << std::endl;

  std::vector<Command> trace = ParseTrace(trace_path);
  if (trace.empty())
    return 1;

  State state(source_model, target_model, std::move(trace));
  if (!state.Execute()) {
    std::cerr << "Failed to execute." << std::endl;
    return 1;
  }
  std::cerr << "Clock: " << state.clock() << std::endl;

  if (!state.IsFinished()) {
    std::cerr << "State is not in the finished." << std::endl;
    return 1;
  }

  std::cout << "Energy: " << state.energy() << std::endl;
  return 0;
}

int ExecuteAssemble(const std::string& tgt, const std::string& trace_path) {
  Model model;
  if (!model.Load(tgt)) {
    return 1;
  }

  return ExecuteInternal(nullptr, &model, trace_path);
}

int ExecuteDisassemble(const std::string& src, const std::string& trace) {
  Model source_model;
  if (!source_model.Load(src))
    return 1;
  Model target_model;
  target_model.SetEmptyModel(source_model.r());
  return ExecuteInternal(&source_model, &target_model, trace);
}

int ExecuteReassemble(const std::string& src, const std::string& tgt,
                      const std::string& trace) {
  Model source_model;
  if (!source_model.Load(src))
    return 1;
  Model target_model;
  if (!target_model.Load(tgt))
    return 1;
  return ExecuteInternal(&source_model, &target_model, trace);
}

int PrintHelp(const std::string& program_name) {
  std::cerr << "How to use: " << program_name
            << " [mode] [src model]? [tgt model]? [input trace]"
            << std::endl;
  return 1;
}

}  // namespace


int main(int argc, char* argv[]) {
  if (argc < 2)
    return PrintHelp(argv[0]);

  std::string mode(argv[1]);
  if (mode == "assemble") {
    if (argc != 4)
      return PrintHelp(argv[0]);
    return ExecuteAssemble(argv[2], argv[3]);
  }
  if (mode == "disassemble") {
    if (argc != 4)
      return PrintHelp(argv[0]);
    return ExecuteDisassemble(argv[2], argv[3]);
  }
  if (mode == "reassemble") {
    if (argc != 5)
      return PrintHelp(argv[0]);
    return ExecuteReassemble(argv[2], argv[3], argv[4]);
  }

  // Unknown mode.
  return PrintHelp(argv[0]);
}
