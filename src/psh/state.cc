#include "state.h"

#include <algorithm>
#include <iostream>
#include <set>

constexpr bool kIsDebug = false;

namespace {

bool IsValidPos(const Vec3& pos, int r) {
  if (pos.x < 0 || r <= pos.x ||
      pos.y < 0 || r <= pos.y ||
      pos.z < 0 || r <= pos.z) {
    std::cerr << "Destination is out of range." << std::endl;
    return false;
  }
  return true;
}

bool IsMovable(const Vec3& pos, const Vec3& d, int r,
               UFMatrix& matrix, std::set<Vec3>* volatile_cells) {
  const Vec3 dest = pos + d;
  if (!IsValidPos(dest, r))
    return false;

  auto sign = [](int v) { return v == 0 ? 0 : (v < 0 ? -1 : 1); };
  const Vec3 step = {sign(d.x), sign(d.y), sign(d.z)};
  for (Vec3 p = pos; ; p += step) {
    if (matrix.IsFull(p)) {
      std::cerr << "There is a full cell on the move: " << p << std::endl;
      matrix.Dump(std::cerr);
      std::cerr << std::endl;
      return false;
    }
    if (!volatile_cells->insert(p).second) {
      std::cerr << "Bot is interfared." << std::endl;
      return false;
    }
    if (p == dest)
      break;
  }
  return true;
}

// TODO: refactor.
using Box = std::tuple<Vec3, Vec3>;
Box CreateBox(const Vec3& pos, const Vec3& fd) {
  int min_x, max_x, min_y, max_y, min_z, max_z;
  std::tie(min_x, max_x) = std::minmax(pos.x, pos.x + fd.x);
  std::tie(min_y, max_y) = std::minmax(pos.y, pos.y + fd.y);
  std::tie(min_z, max_z) = std::minmax(pos.z, pos.z + fd.z);
  return std::make_tuple(Vec3{min_x, min_y, min_z}, Vec3{max_x, max_y, max_z});
}

bool IsBoxContains(const Box& box, const Vec3& pos) {
  return !(pos.x < std::get<0>(box).x || std::get<1>(box).x < pos.x ||
           pos.y < std::get<0>(box).y || std::get<1>(box).y < pos.y ||
           pos.z < std::get<0>(box).z || std::get<1>(box).z < pos.z);
}

}  // namespace

State::State(const Model* source_model, const Model* target_model,
             std::vector<Command> trace)
    : model_(target_model),
      clock_(0),
      current_trace_(0),
      energy_(0),
      harmonics_(false),
      matrix_(model_->r()),
      bots_{Bot(1, Vec3{0, 0, 0},
                {2, 3, 4, 5, 6, 7, 8, 9, 10,
                 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                 31, 32, 33, 34, 35, 36, 37, 38, 39, 40})},
      trace_(std::move(trace))
{
  if (source_model) {
    for (int x = 0; x < source_model->r(); ++x) {
      for (int y = 0; y < source_model->r(); ++y) {
        for (int z = 0; z < source_model->r(); ++z) {
          Vec3 pos{x, y, z};
          if (source_model->filled(pos)) {
            matrix_.Add(pos);
          }
        }
      }
    }
  }

  if (kIsDebug) {
    std::cerr << "State initialized: \n";
    matrix_.Dump(std::cerr);
    std::cerr << std::endl;
  }
}

bool State::IsWellFormed() {
  if (!harmonics_) {
    if (!matrix_.IsGrounded()) {
      std::cerr << "harmonics is Low but not grounded." << std::endl;
      if (kIsDebug) {
        matrix_.Dump(std::cerr);
        std::cerr << std::endl;
      }

      return false;
    }
  }

  // Skip bid check.

  // Bot position check.
  std::set<Vec3> position_set;
  for (const auto& bot : bots_) {
    if (matrix_.IsFull(bot.pos()))
      return false;
    if (!position_set.insert(bot.pos()).second)
      return false;
  }

  // Skip seeds check.

  return true;
}

bool State::IsFinished() const {
  if (harmonics_)
    return false;

  if (!bots_.empty())
    return false;

  if (current_trace_ != trace_.size())
    return false;

  for (int x = 0; x < model_->r(); ++x) {
    for (int y = 0; y < model_->r(); ++y) {
      for (int z = 0; z < model_->r(); ++z) {
        const Vec3 pos{x, y, z};
        if (model_->filled(pos) != matrix_.IsFull(pos)) {
          return false;
        }
      }
    }
  }

  return true;
}

bool State::Execute() {
  while (!bots_.empty()) {
    if (!ExecuteStep()) {
      std::cerr << "Found an error at clock: " << clock_ << std::endl;
      return false;
    }
  }
  return true;
}

bool State::ExecuteStep() {
  if (!IsWellFormed()) {
    std::cerr << "State is not well formed." << std::endl;
    return false;
  }

  if (trace_.size() < current_trace_ + bots_.size()) {
    std::cerr << "Not enough trace is supplied." << std::endl;
    return false;
  }

  // Precondition Check
  {
    std::set<Vec3> volatile_cells;
    int n = bots_.size();
    for (int index = 0; index < n; ++index) {
      const auto& bot = bots_[index];
      const auto& c = trace_[current_trace_ + index];

      switch (c.type()) {
        case Command::Type::Halt:
          if (bots_.size() != 1 || harmonics_ || bot.pos() != Vec3{0, 0, 0}) {
            std::cerr << "Cannot Halt." << std::endl;
            matrix_.Dump(std::cerr);
            std::cerr << std::endl;
            return false;
          }
          // Fall through.
        case Command::Type::Wait:
        case Command::Type::Flip:
          if (!volatile_cells.insert(bot.pos()).second) {
            std::cerr << "bot is interfared." << std::endl;
            return false;
          }
          break;
        case Command::Type::SMove: {
          const auto& lld = c.arg1();
          if (!IsMovable(bot.pos(), lld, model_->r(),
                         matrix_, &volatile_cells)) {
            return false;
          }
          break;
        }
        case Command::Type::LMove: {
          const auto& sld1 = c.arg1();
          const auto& sld2 = c.arg2();
          if (!IsMovable(bot.pos(), sld1, model_->r(),
                         matrix_, &volatile_cells)) {
            return false;
          }
          Vec3 turn_point = bot.pos() + sld1;
          volatile_cells.erase(turn_point);
          if (!IsMovable(turn_point, sld2, model_->r(),
                         matrix_, &volatile_cells)) {
            return false;
          }
          break;
        }
        case Command::Type::FusionP: {
          const auto& nd = c.arg1();
          const auto pos_p = bot.pos();
          const auto pos_s = pos_p + nd;
          if (!IsValidPos(pos_s, model_->r()))
            return false;

          auto iter = std::find_if(
              bots_.begin(), bots_.begin() + n,
              [pos_s](const Bot& bot2) -> bool { return bot2.pos() == pos_s; });
          if (iter == bots_.begin() + n) {
            std::cerr << "BotS is not found." << std::endl;
            return false;
          }
          const auto& c2 = trace_[iter - bots_.begin() + current_trace_];
          if (c2.type() != Command::Type::FusionS) {
            std::cerr << "Corresponding Bot is not FusionS." << std::endl;
            return false;
          }
          if (c2.arg1() + pos_s != pos_p) {
            std::cerr << "Corresponding bot is not fusion to current bot."
                      << std::endl;
            return false;
          }

          if (!volatile_cells.insert(pos_p).second ||
              !volatile_cells.insert(pos_s).second) {
            std::cerr << "bot is interfared." << std::endl;
            return false;
          }
          break;
        }
        case Command::Type::FusionS: {
          const auto& nd = c.arg1();
          const auto pos_s = bot.pos();
          const auto pos_p = pos_s + nd;
          if (!IsValidPos(pos_p, model_->r()))
            return false;

          auto iter = std::find_if(
              bots_.begin(), bots_.begin() + n,
              [pos_p](const Bot& bot2) -> bool { return bot2.pos() == pos_p; });
          if (iter == bots_.begin() + n) {
            std::cerr << "BotP is not found." << std::endl;
            return false;
          }
          const auto& c2 = trace_[iter - bots_.begin() + current_trace_];
          if (c2.type() != Command::Type::FusionP) {
            std::cerr << "Corresponding Bot is not FusionP." << std::endl;
            return false;
          }
          if (c2.arg1() + pos_p != pos_s) {
            std::cerr << "Corresponding bot is not fusion to current bot."
                      << std::endl;
            return false;
          }

          // Volatile cell check is done in the corresponding FusionP.
          break;
        }
        case Command::Type::Fission: {
          const auto& nd = c.arg1();
          int m = c.m();
          const auto pos1 = bot.pos();
          const auto pos2 = pos1 + nd;
          if (!IsValidPos(pos2, model_->r()))
            return false;
          if (matrix_.IsFull(pos2)) {
            std::cerr << "Fission pos is full." << std::endl;
            return false;
          }
          if (bot.seeds().size() < m + 1) {
            std::cerr << "Seeds is not enough." << std::endl;
            return false;
          }
          if (!volatile_cells.insert(pos1).second ||
              !volatile_cells.insert(pos2).second) {
            std::cerr << "Bots are interfared." << std::endl;
            return false;
          }
          break;
        }
        case Command::Type::Fill:
        case Command::Type::Void: {
          const auto& nd = c.arg1();
          const auto& pos = bot.pos() + nd;
          if (!IsValidPos(pos, model_->r()))
            return false;
          if (!volatile_cells.insert(bot.pos()).second ||
              !volatile_cells.insert(pos).second) {
            std::cerr << "Bots are interfared." << std::endl;
            return false;
          }
          break;
        }
        case Command::Type::GFill:
        case Command::Type::GVoid: {
          const auto& nd = c.arg1();
          const auto& corner = bot.pos() + nd;
          const auto& fd = c.arg2();
          if (!IsValidPos(corner, model_->r()))
            return false;
          if (!IsValidPos(corner + fd, model_->r()))
            return false;

          Box box = CreateBox(corner, fd);
          const auto& leader_corner = std::get<0>(box);
          if (corner != leader_corner) {
            // This is not the leader. Just look for a leader bot.
            // Other check is done in the leader bot's check.
            bool found = false;
            for (int leader_index = 0; leader_index < bots_.size();
                 ++leader_index) {
              if (leader_index == index)
                continue;
              const auto& leader = bots_[leader_index];
              const auto& leader_c = trace_[current_trace_ + leader_index];
              if (leader_c.type() != c.type())
                continue;
              const auto& leader_corner2 = leader.pos() + leader_c.arg1();
              if (leader_corner2 == leader_corner &&
                  box == CreateBox(leader_corner2, leader_c.arg2())) {
                found = true;
                break;
              }
            }
            if (!found) {
              std::cerr << "Leader bot is not found." << std::endl;
              return false;
            }
            // Ok we found the leader bot. Do all check with it.
            // Now, break from the switch.
            break;
          }

          // Here, the |bot| is the leader.
          // Collect all bots for the same operataion (incl. the leader).
          std::vector<const Bot*> boxbots;
          std::vector<Vec3> corners;
          for (int i = 0; i < bots_.size(); ++i) {
            const auto& b = bots_[i];
            const auto& c2 = trace_[current_trace_ + i];
            if (c2.type() != c.type())
              continue;
            Vec3 bcorner = b.pos() + c2.arg1();
            if (box == CreateBox(bcorner, c2.arg2())) {
              boxbots.push_back(&b);
              corners.push_back(bcorner);
            }
          }

          int dim_r = (std::get<0>(box).x != std::get<1>(box).x) +
                      (std::get<0>(box).y != std::get<1>(box).y) +
                      (std::get<0>(box).z != std::get<1>(box).z);
          if (boxbots.size() != (1 << dim_r)) {
            std::cerr << "The number of box bots does not match to the box "
                      << "dimension." << std::endl;
            return false;
          }

          std::sort(corners.begin(), corners.end());
          if (std::adjacent_find(corners.begin(), corners.end())
              != corners.end()) {
            std::cerr << "Corner position is not unique." << std::endl;
            return false;
          }

          for (const auto* b : boxbots) {
            if (IsBoxContains(box, b->pos())) {
              std::cerr << "The box contains a bot creating the box."
                        << std::endl;
              return false;
            }
          }

          for (const auto* b : boxbots) {
            if (!volatile_cells.insert(b->pos()).second) {
              std::cerr << "Bots are interfared." << std::endl;
              return false;
            }
          }
          for (int x = std::get<0>(box).x; x <= std::get<1>(box).x; ++x) {
            for (int y = std::get<0>(box).y; y <= std::get<1>(box).y; ++y) {
              for (int z = std::get<0>(box).z; z <= std::get<1>(box).z; ++z) {
                if (!volatile_cells.insert(Vec3{x, y, z}).second) {
                  std::cerr << "Bots are interfared." << std::endl;
                  return false;
                }
              }
            }
          }
          break;
        }
      }
    }
  }

  energy_ += (harmonics_ ? 30 : 3) * model_->r() * model_->r() * model_->r();
  energy_ += 20 * bots_.size();

  // Actual perform.
  {
    int n = bots_.size();
    std::set<int> removed_bid;
    bool dirty = false;
    for (int index = 0; index < n; ++index) {
      auto& bot = bots_[index];
      const Command& c = trace_[current_trace_ + index];

      if (kIsDebug) {
        std::cerr << "Index: " << index << ", "
                  << bot.pos() << ", " << c << std::endl;
      }

      switch (c.type()) {
        case Command::Type::Halt:
          bots_.clear();
          break;
        case Command::Type::Wait:
          // Do nothing.
          break;
        case Command::Type::Flip:
          harmonics_ = !harmonics_;
          break;
        case Command::Type::SMove: {
          const auto& lld = c.arg1();
          bot.Move(lld);
          energy_ += 2 * MLen(lld);
          break;
        }
        case Command::Type::LMove: {
          const auto& sld1 = c.arg1();
          const auto& sld2 = c.arg2();
          bot.Move(sld1);
          bot.Move(sld2);
          energy_ += 2 * (MLen(sld1) + MLen(sld2));
          break;
        }
        case Command::Type::FusionP: {
          const auto& nd = c.arg1();
          auto pos_s = bot.pos() + nd;
          const auto& bot_s = *std::find_if(
              bots_.begin(), bots_.begin() + n,
              [pos_s](const Bot& bot2) -> bool { return bot2.pos() == pos_s; });
          auto* seeds = bot.mutable_seeds();
          seeds->push_back(bot_s.bid());
          seeds->insert(
              seeds->end(), bot_s.seeds().begin(), bot_s.seeds().end());
          std::sort(seeds->begin(), seeds->end());
          energy_ -= 24;
          break;
        }
        case Command::Type::FusionS: {
          removed_bid.insert(bot.bid());
          // Actual remove is performed later.
          break;
        }
        case Command::Type::Fission: {
          const auto& nd = c.arg1();
          int m = c.m();

          auto* seeds = bot.mutable_seeds();
          int new_bid = (*seeds)[0];
          Vec3 new_pos = bot.pos() + nd;
          std::vector<int> new_seeds{
            seeds->begin() + 1, seeds->begin() + m + 1};
          seeds->erase(seeds->begin(), seeds->begin() + m + 1);
          // Note: do not access |bot| hereafter.
          bots_.emplace_back(new_bid, new_pos, std::move(new_seeds));
          dirty = true;
          energy_ += 24;
          break;
        }
        case Command::Type::Fill: {
          const auto& nd = c.arg1();
          auto pos = bot.pos() + nd;
          if (matrix_.IsFull(pos)) {
            energy_ += 6;
          } else {
            matrix_.Add(pos);
            energy_ += 12;
          }
          break;
        }
        case Command::Type::Void: {
          const auto& nd = c.arg1();
          auto pos = bot.pos() + nd;
          if (matrix_.IsFull(pos)) {
            matrix_.Remove(pos);
            energy_ -= 12;
          } else {
            energy_ += 3;
          }
          break;
        }
        case Command::Type::GFill: {
          const auto& nd = c.arg1();
          auto pos = bot.pos() + nd;
          Box box = CreateBox(pos, c.arg2());
          if (pos != std::get<0>(box))
            break;

          for (int x = std::get<0>(box).x; x <= std::get<1>(box).x; ++x) {
            for (int y = std::get<0>(box).y; y <= std::get<1>(box).y; ++y) {
              for (int z = std::get<0>(box).z; z <= std::get<1>(box).z; ++z) {
                Vec3 p {x, y, z};
                if (matrix_.IsFull(p)) {
                  energy_ += 6;
                } else {
                  matrix_.Add(p);
                  energy_ += 12;
                }
              }
            }
          }
          break;
        }
        case Command::Type::GVoid: {
          const auto& nd = c.arg1();
          auto pos = bot.pos() + nd;
          Box box = CreateBox(pos, c.arg2());
          if (pos != std::get<0>(box))
            break;

          for (int x = std::get<0>(box).x; x <= std::get<1>(box).x; ++x) {
            for (int y = std::get<0>(box).y; y <= std::get<1>(box).y; ++y) {
              for (int z = std::get<0>(box).z; z <= std::get<1>(box).z; ++z) {
                Vec3 p {x, y, z};
                if (matrix_.IsFull(p)) {
                  matrix_.Remove(p);
                  energy_ -= 12;
                } else {
                  energy_ += 3;
                }
              }
            }
          }
          break;
        }
      }
    }

    // Maintain bots.
    if (!removed_bid.empty()) {
      bots_.erase(
          std::remove_if(bots_.begin(), bots_.end(),
                         [&removed_bid](const Bot& bot) {
                           return removed_bid.count(bot.bid());
                         }),
          bots_.end());
    }
    if (dirty) {
      std::sort(bots_.begin(), bots_.end(),
                [](const Bot& b1, const Bot& b2) {
                  return b1.bid() < b2.bid();
                });

      if (kIsDebug) {
        for (int i = 0; i < bots_.size(); ++i) {
          std::cerr << "bot[" << i << "]: " << bots_[i] << "\n";
        }
        std::cerr << std::endl;
      }
    }

    current_trace_ += n;
  }

  ++clock_;
  return true;
}
