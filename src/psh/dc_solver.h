#ifndef DC_SOLVER_H_
#define DC_SOLVER_H_

#include "model.h"
#include "command.h"

class DCSolver {
 public:
  explicit DCSolver(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;
};


#endif  // DC_SOLVER_H_
