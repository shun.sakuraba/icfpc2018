#include <iostream>
#include <sstream>

#include "default_solver.h"
#include "dc_solver.h"
#include "dc_solver2.h"
#include "box_deleter.h"
#include "../chun/mission_solver.h"
#include "../kinaba/ki_solver.h"
#include "../kinaba/disolve.h"
#include "model.h"

namespace {

template <typename Solver, typename ... T>
std::vector<Command> Solve(const Model* model, T ... args) {
  Solver solver(model);
  return solver.Solve(args...);
}

int PrintHelp(const std::string& program_name) {
  std::cerr << "How to use: " << program_name
            << " [solver] [mode] [src model]? [tgt model]? [output trace]"
            << std::endl;
  return 1;
}

std::vector<Command> SolveAssemble(const std::string& solver, const Model* model) {
  std::vector<Command> result;
  if (solver == "default") {
    result = Solve<DefaultSolver>(model);
  } else if (solver == "dc") {
    result = Solve<DCSolver>(model);
  } else if (solver == "dc2") {
    result = Solve<DCSolver2>(model);
  } else if (solver.substr(0,2) == "ki") {
    int maxP = 8;
    std::stringstream ss;
    ss << solver.substr(2);
    ss >> maxP;
    result = Solve<KiSolver>(model, maxP);
  } else if (solver == "mission") {
    result = Solve<MissionSolver>(model);
  }
  return result;
}

std::vector<Command> SolveDisassemble(const std::string& solver, const Model* model) {
  return ReverseTrace(model->r(), SolveAssemble(solver, model));
}

int SolveAssemble(const std::string& solver,
                  const std::string& tgt, const std::string& output) {
  Model model;
  if (!model.Load(tgt)) {
    return 1;
  }

  std::vector<Command> result = SolveAssemble(solver, &model);

  if (!OutputTrace(result, output)) {
    return 1;
  }
  return 0;
}

int SolveDisassemble(const std::string& solver,
                     const std::string& src, const std::string& output) {
  Model model;
  if (!model.Load(src)) {
    return 1;
  }

  // TOOD: special solver for non-reversible disassemble
  std::vector<Command> result;
  if (solver == "box") {
    result = Solve<BoxDeleter>(&model);
  } else {
    result = SolveDisassemble(solver, &model);
  }

  if (!OutputTrace(result, output)) {
    return 1;
  }
  return 0;
}

int SolveReassemble(const std::string& solver,
                    const std::string& src, const std::string& tgt,
                    const std::string& output) {
  Model smodel;
  if (!smodel.Load(src)) {
    return 1;
  }
  Model tmodel;
  if (!tmodel.Load(tgt)) {
    return 1;
  }

  // TOOD: special solver for direct reconstruction
  std::vector<Command> result = SolveDisassemble(solver, &smodel);
  if(!result.empty() && result.back().type()==Command::Type::Halt)
    result.pop_back();
  std::vector<Command> result2 = SolveAssemble(solver, &tmodel);
  result.insert(result.end(), result2.begin(), result2.end());
  
  if (!OutputTrace(result, output)) {
    return 1;
  }
  return 0;
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    return PrintHelp(argv[0]);
  }

  std::string mode(argv[2]);
  if (mode == "assemble") {
    if (argc != 5)
      return PrintHelp(argv[0]);
    return SolveAssemble(argv[1], argv[3], argv[4]);
  }
  if (mode == "disassemble") {
    if (argc != 5)
      return PrintHelp(argv[0]);
    return SolveDisassemble(argv[1], argv[3], argv[4]);
  }
  if (mode == "reassemble") {
    if (argc != 6)
      return PrintHelp(argv[0]);
    return SolveReassemble(argv[1], argv[3], argv[4], argv[5]);
  }

  // Unknown mode.
  return PrintHelp(argv[0]);
}
