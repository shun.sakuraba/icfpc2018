#include "../kinaba/disolve.h"
#include "model.h"
#include "command.h"

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr << "How to use: " << argv[0]
              << " [model] [input] [output]" << std::endl;
    return 1;
  }

  Model model;
  if (!model.Load(argv[1])) {
    std::cerr << "Failed to load a model." << std::endl;
    return 1;
  }

  std::vector<Command> trace = ParseTrace(argv[2]);
  if (trace.empty()) {
    std::cerr << "Failed to load a trace." << std::endl;
    return 1;
  }

  std::vector<Command> reversed = ReverseTrace(model.r(), trace);
  if (!OutputTrace(reversed, argv[3])) {
    std::cerr << "Failed to output the reversed trace." << std::endl;
    return 1;
  }

  return 0;
}
