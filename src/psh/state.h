#ifndef STATE_H_
#define STATE_H_

#include "ufmatrix.h"
#include "bot.h"
#include "command.h"
#include "model.h"

#include <cstdint>
#include <vector>

class State {
 public:
  State(const Model* source_model, const Model* target_model,
        std::vector<Command> trace);

  bool IsWellFormed();
  bool IsFinished() const;

  bool Execute();
  bool ExecuteStep();
  std::uint64_t energy() const { return energy_; }
  std::uint64_t clock() const { return clock_; }

  int current_trace() const { return current_trace_; }
  void Flip() { harmonics_ = !harmonics_; }
  // Mutable for IsGrounded().
  UFMatrix& matrix() { return matrix_; }
  const std::vector<Bot>& bots() const { return bots_; }

  template<typename Iter>
  void AppendTrace(Iter s, Iter e) {trace_.insert(trace_.end(), s, e);}

 private:
  const Model* const model_;

  std::uint64_t clock_;
  int current_trace_;

  std::uint64_t energy_;
  bool harmonics_;  // false: low. true: high.
  UFMatrix matrix_;
  std::vector<Bot> bots_;  // Sorted in bid.
  std::vector<Command> trace_;
};

#endif  // STATE_H_
