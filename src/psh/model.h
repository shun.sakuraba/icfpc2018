#ifndef MODEL_H_
#define MODEL_H_

#include "vec3.h"

#include <cstdint>
#include <vector>
#include <string>

class Model {
 public:
  Model();
  ~Model();

  bool Load(const std::string& path);
  void SetEmptyModel(int r);

  std::uint8_t r() const { return r_; }

  bool filled(const Vec3& pos) const {
    int index = pos.x * r_ * r_ + pos.y * r_ + pos.z;
    return (data_[index / 8] >> (index % 8)) & 1;
  }

  int min_x() const { return min_x_; }
  int max_x() const { return max_x_; }
  int min_y() const { return min_y_; }
  int max_y() const { return max_y_; }
  int min_z() const { return min_z_; }
  int max_z() const { return max_z_; }

 private:
  std::uint8_t r_;
  std::vector<std::uint8_t> data_;

  int min_x_;
  int max_x_;
  int min_y_;
  int max_y_;
  int min_z_;
  int max_z_;
};

#endif  // MODEL_H_
