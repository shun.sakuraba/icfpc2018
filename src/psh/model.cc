#include "model.h"

#include <iostream>
#include <iterator>
#include <fstream>

Model::Model() = default;
Model::~Model() = default;

bool Model::Load(const std::string& path) {
  std::ifstream stream(path, std::ios_base::binary);
  if (!stream) {
    std::cerr << "Failed to open a file: " << path << std::endl;
    return false;
  }

  std::istreambuf_iterator<char> iter(stream);
  std::istreambuf_iterator<char> end;

  r_ = *iter;
  data_.clear();
  data_.insert(data_.begin(), ++iter, end);

  // Calculate bounding box.
  min_x_ = max_x_ = min_y_ = max_y_ = min_z_ = max_z_ = -1;
  for (int x = 0; x < r_; ++x) {
    for (int y = 0; y < r_; ++y) {
      for (int z = 0; z < r_; ++z) {
        if (filled(Vec3{x, y, z})) {
          if (min_x_ == -1 || min_x_ > x) min_x_ = x;
          if (max_x_ < x) max_x_ = x;
          if (min_y_ == -1 || min_y_ > y) min_y_ = y;
          if (max_y_ < y) max_y_ = y;
          if (min_z_ == -1 || min_z_ > z) min_z_ = z;
          if (max_z_ < z) max_z_ = z;
        }
      }
    }
  }
  return true;
}

void Model::SetEmptyModel(int r) {
  r_ = r;
  min_x_ = max_x_ = min_y_ = max_y_ = min_z_ = max_z_ = 1; // toriaezu
  data_.assign((r*r*r+7)/8, 0);
}
