#include "urara_solver.h"

#include "../psh/command_util.h"
#include "../psh/model.h"
#include "../psh/model_util.h"
#include "../psh/ufmatrix.h"
#include "../psh/array_by_vec3.h"
#include "../chun/bitmap.h"
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <iostream>
#include <assert.h>

using namespace std;

namespace {

class BotState final {
 public:
  BotState(const Vec3& pos) : pos_(pos) {}
  Vec3 pos() const { return pos_; }
  int t() const { return commands_.size(); }

  void addCommand(const Command& command) { commands_.push_back(command); }
  Vec3& pos() { return pos_; }

 private:
  Vec3 pos_;
  vector<Command> commands_;
  // Next clock that the bot can take next unscheduled action.
};

class WorldState final {
 public:
  WorldState() : r_(0), matrix_(0), conflict_(0) {}

  explicit WorldState(int r) : r_(r), matrix_(r), conflict_(r) {}
  WorldState(const WorldState& other)
      : r_(other.r_), matrix_(other.matrix_), conflict_(other.conflict_) {}

  // Only for NextClock()
  WorldState(int r, const UFMatrix& matrix)
      : r_(r), matrix_(matrix), conflict_(r_) {}

  WorldState NextClock() const { return WorldState(r_, matrix_); }

  UFMatrix& matrix() { return matrix_; }
  const UFMatrix& matrix() const { return matrix_; }
  Bitmap& conflict() { return conflict_; }
  const Bitmap& conflict() const { return conflict_; }

 private:
  const int r_;
  UFMatrix matrix_;
  Bitmap conflict_;
};

typedef vector<WorldState> Worlds;

const WorldState& GetWorld(const Worlds& world, int t) {
  if (t < world.size())
    return world[t];
  else
    return world[world.size() - 1];
}

WorldState& GetMutableWorld(Worlds& world, int t) {
  // We must schedule Fill actions in order.
  // printf("GetMutableWorld %d %d\n", t, (int)world.size());
  assert(t + 1 >= world.size());
  while (t >= world.size()) {
    world.push_back(world[world.size() - 1].NextClock());
  }
  return world[t];
}

#define INDEX(pos, r) ((pos).x * r * r + (pos).y * r + (pos).z)

Command& ensure_backtrack(int r,
                          map<int, vector<Command>>& backtrack,
                          int t,
                          const Vec3& pos) {
  if (backtrack[t].size() == 0)
    backtrack[t] = vector<Command>(r * r * r);
  return backtrack[t][INDEX(pos, r)];
}

static std::vector<Command> bt_helper(const Command& cinit,
                                      int tinit,
                                      map<int, std::vector<Command>> backtrack,
                                      Vec3 pos,
                                      int r,
                                      const Vec3& from) {
  std::vector<Command> ret;
  ret.push_back(cinit);
  int t = tinit;

  while (pos != from) {
    Command c;
    ret.push_back(c = ensure_backtrack(r, backtrack, t, pos));
    --t;
    switch (c.type()) {
      case Command::Type::SMove:
        pos -= c.arg1();
        break;
      case Command::Type::LMove:
        pos -= c.arg2();
        pos -= c.arg1();
        break;
      default:
        assert(false);
        break;
    }
  }

  std::reverse(ret.begin(), ret.end());
  return ret;
}

// XXX: returning vector<Command> is inefficient
// TODO: move semantics or ...?
std::vector<Command> move_to(int r,
                             const Vec3& from,
                             const Vec3& to,
                             const Worlds& worlds,
                             int t_start) {
  // BFS
  std::queue<std::pair<Vec3, int>> q;
  std::vector<Command> ret;
  if (from == to)
    return ret;

  Bitmap visited(r);

  map<int, vector<Command>> backtrack;  // t -> backtrack

  vector<Command> backtrack_;

  q.push(std::make_pair(from, t_start));
  while (!q.empty()) {
    auto pp = q.front();
    q.pop();
    const Vec3 pos = pp.first;
    int t = pp.second;

    // SMove x y z
    for (int dim = 0; dim < 3; ++dim) {
      for (int d : {-1, 1}) {
        for (int l = 1; l <= 15; ++l) {
          int dx = 0, dy = 0, dz = 0;
          switch (dim) {
            case 0:
              dx = l * d;
              break;
            case 1:
              dy = l * d;
              break;
            case 2:
              dz = l * d;
              break;
          }
          Vec3 newpos = pos + Vec3{dx, dy, dz};
          if (!newpos.IsValid(r))
            break;
          if (GetWorld(worlds, t).matrix().test(newpos) ||
              GetWorld(worlds, t).conflict().test(newpos))
            break;
          Command c = Command::SMove(dx, dy, dz);
          if (newpos == to) {
            return bt_helper(c, t, backtrack, pos, r, from);
          }
          // if (ensure_backtrack(r, backtrack, t + 1, newpos).type() !=
          // Command::Type::Halt)
          if (visited.test(newpos))
            continue;
          visited.fill(newpos);
          ensure_backtrack(r, backtrack, t + 1, newpos) = c;
          q.push(std::make_pair(newpos, t + 1));
        }
      }
    }
    // LMove
    for (int dim1 = 0; dim1 < 3; ++dim1) {
      for (int d1 : {-1, 1}) {
        for (int l1 = 1; l1 <= 5; ++l1) {
          int dx1 = 0, dy1 = 0, dz1 = 0;
          switch (dim1) {
            case 0:
              dx1 = l1 * d1;
              break;
            case 1:
              dy1 = l1 * d1;
              break;
            case 2:
              dz1 = l1 * d1;
              break;
          }
          Vec3 newpos1 = pos + Vec3{dx1, dy1, dz1};
          if (!newpos1.IsValid(r))
            break;
          if (GetWorld(worlds, t).matrix().test(newpos1) ||
              GetWorld(worlds, t).conflict().test(newpos1))
            break;
          for (int dim2 = 0; dim2 < 3; ++dim2) {
            if (dim1 == dim2)
              continue;
            for (int d2 : {-1, 1}) {
              for (int l2 = 1; l2 <= 5; ++l2) {
                int dx2 = 0, dy2 = 0, dz2 = 0;
                switch (dim2) {
                  case 0:
                    dx2 = l2 * d2;
                    break;
                  case 1:
                    dy2 = l2 * d2;
                    break;
                  case 2:
                    dz2 = l2 * d2;
                    break;
                }
                Vec3 newpos2 = newpos1 + Vec3{dx2, dy2, dz2};
                if (!newpos2.IsValid(r))
                  break;
                if (GetWorld(worlds, t).matrix().test(newpos2) ||
                    GetWorld(worlds, t).conflict().test(newpos2))
                  break;
                Command c = Command::LMove(dx1, dy1, dz1, dx2, dy2, dz2);
                if (newpos2 == to) {
                  return bt_helper(c, t, backtrack, pos, r, from);
                }
                // if (ensure_backtrack(r, backtrack, t + 1, newpos2).type() !=
                // Command::Type::Halt)
                if (visited.test(newpos2))
                  continue;
                visited.fill(newpos2);
                ensure_backtrack(r, backtrack, t + 1, newpos2) = c;
                q.push(std::make_pair(newpos2, t + 1));
              }
            }
          }
        }
      }
    }
  }
  return std::vector<Command>();
}

vector<Command> ScheduleSingleFill(const Model& model,
                                   const Worlds& world,
                                   const BotState& bot,
                                   const Vec3& target,
                                   const Vec3& target_bot_pos) {
  vector<Command> commands =
      move_to(model.r(), bot.pos(), target_bot_pos, world, bot.t());
  if (commands.empty())
    return commands;

  // commands.push_back(Command::SMove(target - bot.pos()));
  commands.push_back(Command::Fill(target - target_bot_pos));
  return commands;
}

void Apply(const Model& model,
           Worlds& worlds,
           BotState* bot,
           const vector<Command>& schedule) {
  for (const Command& command : schedule) {
    int t = bot->t();
    WorldState& world = GetMutableWorld(worlds, t);
    // TODO: Mark conflicts on world.
    if (command.type() == Command::Type::Fill) {
      const Vec3 pos = bot->pos() + command.arg1();
      world.matrix().Add(pos);
    }
    if (command.type() == Command::Type::SMove) {
      bot->pos() += command.arg1();
    }
    if (command.type() == Command::Type::LMove) {
      bot->pos() += command.arg1();
      bot->pos() += command.arg2();
    }
    bot->addCommand(command);
  }
}

bool SolveInternal(const Model& model,
                   const vector<vector<pair<Vec3, Vec3>>>& targets,
                   vector<vector<Command>>& results) {
  // |matrix| represents the matrix at tim
  UFMatrix matrix(model.r());

  Worlds world;  // world[t] = world at clock t
  world.push_back(WorldState(model.r()));

  vector<BotState> bots;
  for (int i = 0; i < 1; ++i) {
    bots.push_back(BotState(Vec3{i, 0, 0}));
  }

  vector<vector<pair<Vec3, Vec3>>::const_iterator> curr_targets;
  for (int i = 0; i < targets.size(); ++i) {
    curr_targets.push_back(targets[i].begin());
  }

  std::vector<Command> result;
  int t_last_scheduled = -1;
  while (true) {
    // Pre-schedule each bot individually.
    const int T_INF = 100000000;
    int t_min = T_INF;
    bool still_need_scheduling = false;

    printf("----------------------\n");

    for (int i = 0; i < targets.size(); ++i) {
      if (curr_targets[i] == targets[i].end())
        break;
      still_need_scheduling = true;
      vector<Command> next =
          ScheduleSingleFill(model, world, bots[i], curr_targets[i]->first,
                             curr_targets[i]->second);
      printf(
          "Scheduling bot[%d]: from (%d,%d,%d) at time %d => (%d,%d,%d) w/ %d "
          "steps\n",
          i, bots[i].pos().x, bots[i].pos().y, bots[i].pos().z, bots[i].t(),
          curr_targets[i]->first.x, curr_targets[i]->first.y,
          curr_targets[i]->first.z, (int)next.size());
      for (const Command& command : next) {
        std::cout << command << endl;
      }
      if (next.empty())
        continue;
      int t = bots[i].t() + (int)next.size();
      if (!GetWorld(world, t).matrix().IsAdjacentToGrounded(
              curr_targets[i]->first)) {
        printf("(%d,%d,%d) is adj-grounded at time %d\n",
               curr_targets[i]->first.x, curr_targets[i]->first.y,
               curr_targets[i]->first.z, t);
        continue;
      }
      t_min = min(t_min, t);
    }

    // Successfully sheduled all bots.
    if (!still_need_scheduling) {
      printf("Success!\n");
      return true;
    }

    printf("Scheduling t_min = %d\n", t_min);

    // No bots can be scheduled.
    // TODO: continue scheduling with ignoring conflicts, and returns the number
    // of ignored conflicts as a signal for optimization.
    if (t_min == 100000000)
      return false;

    // At least one bot is scheduled at t_min in total.
    bool any_scheduled = false;

    // Actually schedule bots, at the earliest time (t clock after the present
    // time).
    // Continue scheduling while there is at least one bot scheduled, because
    // scheduling a bot/fill can enable subsequent fill due to extended
    // connectivity.
    while (true) {
      // Try to schedule the bots that can fill at clock t_min.

      // At least one bot is scheduled in the inner loop.
      bool scheduled = false;
      for (int i = 0; i < targets.size(); ++i) {
        // Already finished.
        if (curr_targets[i] == targets[i].end())
          continue;

        // Not scheduled due to lack of connectivity; trying next t_min might
        // schedule this bot because more voxels will be filled at that time.
        if (!GetWorld(world, t_min + 1)
                 .matrix()
                 .IsAdjacentToGrounded(curr_targets[i]->first)) {
          printf("bot[%d]: Skip due to connectivity\n", i);
          continue;
        }

        vector<Command> actual_schedule =
            ScheduleSingleFill(model, world, bots[i], curr_targets[i]->first,
                               curr_targets[i]->second);
        int t1 = actual_schedule.size();

        // Not scheduled perhaps due to conflict; waiting might schedule this
        // bot.
        if (!t1) {
          printf("bot[%d]: Fail in actual schedule\n", i);
          continue;
        }

        t1 += bots[i].t();
        assert(t1 >= t_min);

        // Not scheduled at t_min; trying next t_min might schedule this bot.
        if (t1 > t_min) {
          printf("bot[%d]: To be scheduled at %d\n", i, t1);
          continue;
        }

        printf("bot[%d]: Scheduled\n", i);
        Apply(model, world, &bots[i], actual_schedule);
        ++curr_targets[i];
        scheduled = true;
        any_scheduled = true;
        t_last_scheduled = max(t_last_scheduled, t_min);
      }

      if (!scheduled)
        break;
    }

    // Some bots are scheduled; proceed to next t_min.
    if (any_scheduled)
      continue;

    {
      // Insert wait for the bots with earliest schedulable time, i.e. abandon
      // scheduling at that timeslot.
      int t_min = T_INF;
      for (int i = 0; i < targets.size(); ++i) {
        int t = bots[i].t();
        t_min = min(t_min, t);
      }
      // No conflicts after t_last_scheduled, but couldn't schedule, i.e.
      // unreachable due to filled blocks, probably. Abandon.
      if (t_last_scheduled < t_min) {
        printf(
            "Couldn't Wait: all bots time (>= %d) is after last-scheduled %d\n",
            t_min, t_last_scheduled);
        return false;
      }

      for (int i = 0; i < targets.size(); ++i) {
        if (bots[i].t() == t_min) {
          printf("bot[%d]: wait at time %d\n", i, bots[i].t());
          bots[i].addCommand(Command::Wait());
        }
      }
    }
  }
}

}  // namespace

UraraSolver::UraraSolver(const Model* model) : model_(model) {}

Vec3 SelectRandomNearPos(int r, const Vec3& pos) {
  int i = 0;
  Vec3 ret;
  for (int dx = -1; dx <= 1; ++dx) {
    for (int dy = -1; dy <= 1; ++dy) {
      for (int dz = -1; dz <= 1; ++dz) {
        Vec3 vec{dx, dy, dz};
        if (CLen(vec) == 1 && (MLen(vec) == 1 || MLen(vec) == 2)) {
          if (vec.IsValid(r) && rand() % ++i == 0)
            ret = pos + vec;
        }
      }
    }
  }
  return ret;
}

vector<Vec3> GetTopologicalOrder(const Model& model) {
  const int r = model.r();
  Bitmap visited(r);
  set<Vec3> wl;

  for (int x = 0; x < r; ++x) {
    for (int z = 0; z < r; ++z) {
      if (model.filled(Vec3{x, 0, z})) {
          wl.insert(Vec3{x, 0, z});
      }
    }
  }
  std::random_device seed_gen;
  std::mt19937 engine(seed_gen());
  // shuffle(targets.begin(), targets.end(), engine);

  vector<Vec3> order;
  while(!wl.empty()) {
    Vec3 pos;
    int i = 0;
    for (const auto& it : wl) {
      if (rand() % ++i == 0)
        pos = it;
    }
    wl.erase(pos);
    if (visited.test(pos))
      continue;
    visited.fill(pos);

    order.push_back(pos);
    for (int dx = -1; dx <= 1; ++dx) {
    for (int dy = -1; dy <= 1; ++dy) {
    for (int dz = -1; dz <= 1; ++dz) {
      Vec3 d{dx, dy, dz};
      if (MLen(d) != 1) continue;
      d += pos;
      if (!d.IsValid(r)) continue;
      if (model.filled(d))
        wl.insert(d);
    }}}
  }
  return order;
}

std::vector<Command> UraraSolver::Solve() {
  while (true) {
    vector<vector<pair<Vec3, Vec3>>> targets(1);
    for (const auto pos : GetTopologicalOrder(*model_)) {
      Vec3 bot_pos = SelectRandomNearPos(model_->r(), pos);
      targets[0].push_back(make_pair(pos, bot_pos));
    }
    //shuffle(targets[0].begin(), targets[0].end(), engine);
    vector<vector<Command>> results;
    printf("==========================\n");
    bool r = SolveInternal(*model_, targets, results);
    if (r) {
      return results[0];
    }
    printf("Fail\n");
  }
}

/*
int main() {
  Model model(32);
  model.FillForTest(Vec3{1, 0, 1});
  model.FillForTest(Vec3{1, 1, 1});

  UraraSolver solver(&model);
  std::vector<Command> result = solver.Solve();
  OutputTrace(result, "foobar.out");
}
*/
