#include "../psh/model.h"
#include "../psh/command.h"

class UraraSolver final {
 public:
  explicit UraraSolver(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;
};
