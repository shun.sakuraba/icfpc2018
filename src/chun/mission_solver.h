#ifndef MISSION_SOLVER_H_
#define MISSION_SOLVER_H_

#include "../psh/model.h"
#include "../psh/command.h"

class MissionSolver {
 public:
  explicit MissionSolver(const Model* model);

  std::vector<Command> Solve();

 private:
  const Model* const model_;
};


#endif  // MISSION_SOLVER_H_
