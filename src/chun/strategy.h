#ifndef STRATEGY_H_
#define STRATEGY_H_


#include "frontier_model.h"

struct FillPos {
  Vec3 fill;
  Vec3 pos;
};

class Strategy {
 public:
  Strategy();
  ~Strategy();

  void generate_initial_strategy(const Model *model, int n);

  // check deadlock ordering
  bool is_valid();

  const std::vector<std::vector<FillPos> >& bid_orders() const {
    return bid_orders_;
  }

 private:
  const Model *m_;
  std::vector<std::vector<FillPos> > bid_orders_;
};

#endif
