#ifndef FRONTIER_MODEL_H_
#define FRONTIER_MODEL_H_

#include <vector>
#include <unordered_set>
#include <functional>

#include "../psh/model.h"
#include "bitmap.h"

struct HashVec3 {
  typedef std::size_t result_type;
  
  result_type operator() (const Vec3& v) const {
    return std::hash<int>{}(v.x) ^ std::hash<int>{}(v.y) ^ std::hash<int>{}(v.z);
  }
};

class FrontierModel {
 public:
  FrontierModel();
  ~FrontierModel();

  void set_model(const Model *m);

  const std::unordered_set<Vec3, HashVec3>& get_frontier() const {
    return frontier_;
  }

  const Bitmap& current_grounded() const { return bm_; }

  std::uint8_t r() const { return model().r(); }
  
  void fill(const Vec3& pos);
  bool filled(const Vec3& pos) const {
    return bm_.test(pos);
  }

  const Model& model() const {
    return *m_;
  }

 private:
  const Model* m_;
  std::unordered_set<Vec3, HashVec3> frontier_;
  Bitmap bm_;

  bool valid(const Vec3& pos) {
    return (pos.x >= 0 && pos.y >= 0 && pos.z >= 0 &&
	    pos.x < r() && pos.y < r() && pos.z < r());
  }
};

#endif // FRONTIER_MODEL_H_
