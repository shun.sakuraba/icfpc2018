#include <algorithm>
#include "strategy.h"

Strategy::Strategy() = default;
Strategy::~Strategy() = default;

// Approx no. of turns to arrive
static int cost(const Vec3& rel)
{
  // TODO: replace with a real one
  if(rel.x == 0 || rel.y == 0 || rel.z == 0) return 1;

  return 2;
}

void Strategy::generate_initial_strategy(const Model* model, int n)
{
  FrontierModel f;

  m_ = model;
  f.set_model(m_);

  std::vector<Vec3> cur_coordinates(n, Vec3{0, 0, 0});

  bid_orders_.clear();
  bid_orders_.resize(n);

  bool finished = false;
  while(true) {
    for(int i = 0; i < n; ++i) {
      const auto& candidates = f.get_frontier();
      if(candidates.empty()) { finished = true; break; }
      Vec3 selected;
      int best = 3 * m_->r();
      int lowest_y = m_->r() + 1;
      for(const auto &c: candidates) {
        int score = cost(c - cur_coordinates[i]);
        if(c.y < lowest_y || (c.y == lowest_y && score < best)) {
          selected = c;
          best = score;
          lowest_y = c.y;
        }
      }

      Vec3 position;
      position = selected;
      position.y += 1; // TODO: reserve position

      cur_coordinates[i] = position;

      bid_orders_[i].emplace_back(FillPos{ selected, position });
      f.fill(selected);
    }
    if(finished) break;
  }
}

bool Strategy::is_valid()
{
  FrontierModel f;
  f.set_model(m_);

  int n = (int)bid_orders_.size();
  std::vector<size_t> ptrs(bid_orders_.size(), size_t(0));

  while(true) {
    bool progress = false;
    for(int i = 0; i < n; ++i) {
      const auto& candidates = f.get_frontier();
      if(candidates.empty()) return true;
      if(ptrs[i] == bid_orders_[i].size()) continue;
      FillPos fp = bid_orders_[i][ptrs[i]];
      if(candidates.find(fp.fill) != candidates.end()) {
        f.fill(fp.fill);
        ++ptrs[i];
        progress = true;
      }
    }
    if(!progress) return false;
  }
}
