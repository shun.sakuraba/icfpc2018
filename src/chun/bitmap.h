#ifndef BITMAP_H_
#define BITMAP_H_

#include <vector>
#include "../psh/model.h"

class Bitmap{
public:
  Bitmap() = default;
  Bitmap(int r) : r_(r), bm_((r * r * r + 8 - 1) / 8, (std::uint8_t)0) {
  }
  Bitmap(const Model &m): r_(m.r()), bm_((r_ * r_ * r_ + 8 - 1) / 8, (std::uint8_t)0) {
    for(int x = 0; x < r_; ++x) {
      for(int y = 0; y < r_; ++y) {
        for(int z = 0; z < r_; ++z) {
          Vec3 pos = {x, y, z};
          if(m.filled(pos)) fill(pos);
        }
      }
    }
      
  }
  Bitmap(const Bitmap &b) = default;
  ~Bitmap() = default;

  void clear(int r){ r_ = r; bm_.clear(); bm_.resize((r * r * r + 8 - 1) / 8, (std::uint8_t)0); }
  std::uint8_t r() const { return r_; }
  
  void fill(const Vec3& pos) {
    int index = pos.x * r() * r() + pos.y * r() + pos.z;
    bm_[index / 8] |= 1u << (index % 8);
  }

  void erase(const Vec3& pos) {
    int index = pos.x * r() * r() + pos.y * r() + pos.z;
    bm_[index / 8] &= ~(1u << (index % 8));
  }    
  
  bool test(const Vec3& pos) const {
    int index = pos.x * r() * r() + pos.y * r() + pos.z;
    return (bm_[index / 8] >> (index % 8)) & 1;
  }

private:
  int r_;
  std::vector<std::uint8_t> bm_;
};

#endif
