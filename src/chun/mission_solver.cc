#include "strategy.h"

#include <queue>
#include <iostream>
#include <algorithm>
#include <cassert>

#include "mission_solver.h"

static std::vector<Command> bt_helper(const Command &cinit, std::vector<Command> backtrack, Vec3 pos, int r, const Vec3 &from)
{
  std::vector<Command> ret;
  ret.push_back(cinit);

  while(pos != from) {
    Command c;
    ret.push_back(c = backtrack[pos.x * r * r + pos.y * r + pos.z]);
    switch(c.type()) {
    case Command::Type::SMove:
      pos -= c.arg1();
      break;
    case Command::Type::LMove:
      pos -= c.arg2();
      pos -= c.arg1();
      break;
    default:
      assert(false);
      break;
    }
  }

  std::reverse(ret.begin(), ret.end());
  return ret;
}

static bool valid_pos(Vec3 pos, int r)
{
  return (pos.x >= 0 && pos.y >= 0 && pos.z >= 0 &&
          pos.x < r && pos.y < r && pos.z < r);
}

// XXX: returning vector<Command> is inefficient
// TODO: move semantics or ...?
std::vector<Command> move_to(const Vec3& from, const Vec3& to, const Bitmap &fill_bm, const Bitmap &volatile_bm)
{
  int r = fill_bm.r();
  Bitmap visited(r);
  // BFS
  std::vector<Command> backtrack(r * r * r);
  std::queue<std::pair<Vec3, int> > q;
  std::vector<Command> ret;
  if(from == to) return ret;
  q.push(std::make_pair(from, 0));
  while(!q.empty()) {
    auto p = q.front();
    q.pop();

    // SMove x y z
    for(int dim = 0; dim < 3; ++dim) {
      for(int d: {-1, 1}) {
        for(int l = 1; l <= 15; ++l) {
          int dx = 0, dy = 0, dz = 0;
          switch(dim) {
          case 0: dx = l * d; break;
          case 1: dy = l * d; break;
          case 2: dz = l * d; break;
          }
          Vec3 newpos = p.first + Vec3{dx, dy, dz};
          if(!valid_pos(newpos, r)) break;
          if(fill_bm.test(newpos) || volatile_bm.test(newpos)) break;
          if(visited.test(newpos)) continue;
          Command c = Command::SMove(dx, dy, dz);
          if(newpos == to) { return bt_helper(c, backtrack, p.first, r, from); }
          visited.fill(newpos);
          backtrack[newpos.x * r * r + newpos.y * r + newpos.z] = c;
          q.push(std::make_pair(newpos, p.second + 1));
        }
      }
    }
    // LMove
    for(int dim1 = 0; dim1 < 3; ++dim1) {
      for(int d1: {-1, 1}) {
        for(int l1 = 1; l1 <= 5; ++l1) {
          int dx1 = 0, dy1 = 0, dz1 = 0;
          switch(dim1) {
          case 0: dx1 = l1 * d1; break;
          case 1: dy1 = l1 * d1; break;
          case 2: dz1 = l1 * d1; break;
          }
          Vec3 newpos1 = p.first + Vec3{dx1, dy1, dz1};
          if(!valid_pos(newpos1, r)) break;
          if(fill_bm.test(newpos1) || volatile_bm.test(newpos1)) break;
          for(int dim2 = 0; dim2 < 3; ++dim2) {
            if(dim1 == dim2) continue;
            for(int d2: {-1, 1}) {
              for(int l2 = 1; l2 <= 5; ++l2) {
                int dx2 = 0, dy2 = 0, dz2 = 0;
                switch(dim2) {
                case 0: dx2 = l2 * d2; break;
                case 1: dy2 = l2 * d2; break;
                case 2: dz2 = l2 * d2; break;
                }
                Vec3 newpos2 = newpos1 + Vec3{dx2, dy2, dz2};
                if(!valid_pos(newpos2, r)) break;
                if(fill_bm.test(newpos2) || volatile_bm.test(newpos2)) break;
                if(visited.test(newpos2)) continue;
                Command c = Command::LMove(dx1, dy1, dz1, dx2, dy2, dz2);
                if(newpos2 == to) { return bt_helper(c, backtrack, p.first, r, from); }
                visited.fill(newpos2);
                backtrack[newpos2.x * r * r + newpos2.y * r + newpos2.z] = c;
                q.push(std::make_pair(newpos2, p.second + 1));
              }
            }
          }
        }
      }
    }
  }
  return std::vector<Command>();
}

static int sgn(int x)
{
  if(x > 0) return 1;
  if(x < 0) return -1;
  return 0;
}

void update_pos_volatile_bm(const Command& c, Vec3* curpos, Bitmap *volatile_bm)
{
  switch(c.type()) {
  case Command::Type::SMove:
    {
      Vec3 d = c.arg1();
      if(d.x == 0 && d.y == 0) {
        int s = sgn(d.z);
        for(int i = 0; i <= abs(d.z); ++i) {
          Vec3 nc = *curpos + Vec3{0, 0, s * i};
          volatile_bm->fill(nc);
        }
      }else if(d.x == 0 && d.z == 0) {
        int s = sgn(d.y);
        for(int i = 0; i <= abs(d.y); ++i) {
          Vec3 nc = *curpos + Vec3{0, s * i, 0};
          volatile_bm->fill(nc);
        }
      }else if(d.y == 0 && d.z == 0) {
        int s = sgn(d.x);
        for(int i = 0; i <= abs(d.x); ++i) {
          Vec3 nc = *curpos + Vec3{s * i, 0, 0};
          volatile_bm->fill(nc);
        }
      }
      *curpos += d;
    }
    break;
  case Command::Type::LMove:
    {
      Vec3 d1 = c.arg1();
      if(d1.x == 0 && d1.y == 0) {
        int s = sgn(d1.z);
        for(int i = 0; i <= abs(d1.z); ++i) {
          Vec3 nc = *curpos + Vec3{0, 0, s * i};
          volatile_bm->fill(nc);
        }
      }else if(d1.x == 0 && d1.z == 0) {
        int s = sgn(d1.y);
        for(int i = 0; i <= abs(d1.y); ++i) {
          Vec3 nc = *curpos + Vec3{0, s * i, 0};
          volatile_bm->fill(nc);
        }
      }else if(d1.y == 0 && d1.z == 0) {
        int s = sgn(d1.x);
        for(int i = 0; i <= abs(d1.x); ++i) {
          Vec3 nc = *curpos + Vec3{s * i, 0, 0};
          volatile_bm->fill(nc);
        }
      }
      *curpos += d1;
      Vec3 d2 = c.arg2();
      if(d2.x == 0 && d2.y == 0) {
        int s = sgn(d2.z);
        for(int i = 0; i <= abs(d2.z); ++i) {
          Vec3 nc = *curpos + Vec3{0, 0, s * i};
          volatile_bm->fill(nc);
        }
      }else if(d2.x == 0 && d2.z == 0) {
        int s = sgn(d2.y);
        for(int i = 0; i <= abs(d2.y); ++i) {
          Vec3 nc = *curpos + Vec3{0, s * i, 0};
          volatile_bm->fill(nc);
        }
      }else if(d2.y == 0 && d2.z == 0) {
        int s = sgn(d2.x);
        for(int i = 0; i <= abs(d2.x); ++i) {
          Vec3 nc = *curpos + Vec3{s * i, 0, 0};
          volatile_bm->fill(nc);
        }
      }
      *curpos += d2;
    }
    break;
  default:
    assert(!"update_pos_volatile_bm: unknown command.");
    break;
  }
}

// FIXME: hey, please clean this mess up...
void update_pos_volatile_bm(const std::vector<Command>& cs, Vec3* curpos, Bitmap *volatile_bm)
{
  for(const auto& c: cs) {
    update_pos_volatile_bm(c, curpos, volatile_bm);
  }
}

MissionSolver::MissionSolver(const Model* model) : model_(model) {}

std::vector<Command> MissionSolver::Solve() {
  const int n = 1;

  std::vector<Command> output;
  std::vector<Vec3> curbotpos(n, Vec3{0, 0, 0});
  
  FrontierModel f;
  f.set_model(model_);
  
  // fission
  if(n != 1) {
    assert(false);
  }

  Strategy strat;

  strat.generate_initial_strategy(model_, n);
  
  std::vector<std::vector<FillPos> > bids = strat.bid_orders();
  std::vector<size_t> ptrs(n, (size_t)0);
  std::cout << "Debug: nbids = " << bids[0].size() << std::endl;

  std::vector<std::queue<Command> > command_queue;
  int r = model_->r();
  Bitmap volatile_bm(r);
  while(true) {
    // volatile bm is cleared every turn.
    volatile_bm.clear(r);
    const auto &frontier = f.get_frontier();
    if(frontier.empty()) break;
    for(int ibot = 0; ibot < n; ibot++) {
      // for each bot, try to make progress on their own bids.
      if(ptrs[ibot] == bids[ibot].size()) {
        output.push_back(Command::Wait());
        //std::cout << "Debug: bot " << ibot << " waiting" << std::endl;
      }else{
        FillPos fp = bids[ibot][ptrs[ibot]];
        std::cout << "Debug: bot " << ibot << " targets" << fp.pos << std::endl;
        if(frontier.find(fp.fill) == frontier.end()) {
          //std::cout << "Debug: bot " << ibot << " failed to find in frontier" << std::endl;
          output.push_back(Command::Wait());
        }else if(fp.pos == curbotpos[ibot]) {
          // Completed one objective
          std::cout << "Debug: bot " << ibot << " fills" << fp.fill << std::endl;
          Vec3 d = fp.fill - fp.pos;
          output.push_back(Command::Fill(d.x, d.y, d.z));
          f.fill(fp.fill);
          ++ptrs[ibot];
        }else{
          std::vector<Command> mc =
            move_to(curbotpos[ibot], fp.pos, f.current_grounded(), volatile_bm);
          if(mc.empty()) {
            // no way to go to the target
            
            output.push_back(Command::Halt());
            assert(!"Solver failed: no way to go to specified position (buried?)");
          }else{
            // execute only the first command from the back-tracked result.
            // This is necessary to handle commands with different lengths.
            std::cout << "Debug: bot action" << mc[0] << std::endl;
            output.push_back(mc[0]);
            update_pos_volatile_bm(mc[0], &curbotpos[ibot], &volatile_bm);
            std::cout << "Debug: bot moved to " << curbotpos[ibot] << std::endl;
          }
        }
      }
    }
  }

  // fusion and return to the origin
  if(n != 1) {
    assert(false);
  }

  return output;
}

