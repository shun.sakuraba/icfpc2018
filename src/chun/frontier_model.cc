#include <algorithm>
#include <cassert>

#include "frontier_model.h"

FrontierModel::FrontierModel() = default;
FrontierModel::~FrontierModel() = default;

void FrontierModel::set_model(const Model* m)
{
  m_ = m;
  int r = (unsigned int)model().r();
  bm_.clear(r);

  frontier_.clear();
  for(int x = 0; x < r; ++x) {
    for(int z = 0; z < r; ++z) {
      if(model().filled(Vec3{x, 0, z})) {
        frontier_.insert(Vec3{ x, 0, z});
      }
    }
  }
}

void FrontierModel::fill(const Vec3& pos)
{
  auto it = frontier_.find(pos);
  assert(it != frontier_.end());
  frontier_.erase(it);
  bm_.fill(pos);
  
  // From "Grounded":
  // if either y = 0 or there exists an adjacent Full coordinate c′ = c + d (where mlen(d) = 1)
  const Vec3 deltax[] = {
    {  1,  0,  0 }, { -1,  0,  0 },
    {  0,  1,  0 }, {  0, -1,  0 },
    {  0,  0,  1 }, {  0,  0, -1 }
  };

  for(const auto d: deltax) {
    Vec3 posp = pos + d;
    if(valid(posp) && model().filled(posp) && !filled(posp)) {
      if(frontier_.find(posp) != frontier_.end()) {
        frontier_.insert(posp);
      }
    }
  }
}

