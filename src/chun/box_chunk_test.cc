#include<iostream>
#include<sstream>
#include<iomanip>

#include "box_chunk.h"
#include "bitmap.h"
#include "../psh/model.h"

int main(int argc, char* argv[])
{
  for(int modelno = 170; modelno <= 186; ++modelno) {
    std::ostringstream os;
    // hard-coding path, extremely bad practice I know
    os << "../../data/problemsL/LA";
    os << std::setw(3) << std::setfill('0') << modelno << "_tgt.mdl";
    Model m;
    m.Load(os.str());
    Bitmap bm(m);
    BoxChunk bc;
    bc.set_model(&bm);
    BoxRange br = bc.find_chunk();

    std::cout << os.str() << ": " << br << std::endl;
  }


  return 0;
}
