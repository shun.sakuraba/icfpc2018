

#include "box_chunk.h"
#include <vector>
#include <stack>

BoxRange BoxChunk::find_chunk()
{
  // FIXME: This routine takes O(N^4) time, but I strongly think that it can be finished in O(N^3).
  int r = remain_.r();
  std::vector<int> scan_counts(r * r, 0);
  std::vector<int> scan1d(r);

  std::stack<int> hist_stack;

  int maxvolume = -1;
  BoxRange maxrange = { -1, -1, -1, -1, -1, -1 };

  for(int x = 0; x < r; ++x) {
    for(int y = 0; y < r; ++y) {
      for(int z = 0; z < r; ++z) {
        size_t ptr = y * r + z;
        scan_counts[ptr] = remain_.test(Vec3{x, y, z}) ? scan_counts[ptr] + 1 : 0;
      }
    }

    int countmax = *std::max_element(scan_counts.begin(), scan_counts.end());
    // We then need to maximize RMQ2D(xrange, yrange) * area(xrange, yrange). 

    // make 0-1 binary matrix s.t. scan_counts[x, y] >= k
    // Then we solve the same problem but in 2D. Fortunately there exists a solution in O(N^2).
    // combined with the scanning in k, this part takes O(N^3).
    for(int k = 1; k <= countmax; ++k) {
      std::fill(scan1d.begin(), scan1d.end(), 0);
      for(int y = 0; y < r; ++y) {
        for(int z = 0; z < r; ++z) {
          scan1d[z] = scan_counts[y * r + z] >= k ? scan1d[z] + 1 : 0;
        }
        // max area under histogram for scan1d. O(N).
        for(int z = 0; z < r; ++z) {
          if(hist_stack.empty() || scan1d[z] >= scan1d[hist_stack.top()]) {
            hist_stack.push(z);
          }else{
            while(!hist_stack.empty() && scan1d[z] < scan1d[hist_stack.top()]) {
              int lowest_in_range;
              lowest_in_range = hist_stack.top();
              hist_stack.pop();
              int zbeg = hist_stack.empty() ? 0 : hist_stack.top() + 1;
              int volume = k * scan1d[lowest_in_range] * (z - zbeg);
              if(volume > maxvolume) {
                maxvolume = volume;
                maxrange = BoxRange{ x - k + 1, x + 1,
                                     y - scan1d[lowest_in_range] + 1, y + 1,
                                     zbeg, z};
              }
            }
          }
        }
        while(!hist_stack.empty()) {
          int lowest_in_range;
          lowest_in_range = hist_stack.top();
          hist_stack.pop();
          int zbeg = hist_stack.empty() ? 0 : hist_stack.top() + 1;
          int volume = k * scan1d[lowest_in_range] * (r - zbeg);
          if(volume > maxvolume) {
            maxvolume = volume;
            maxrange = BoxRange{ x - k + 1, x + 1,
                                 y - scan1d[lowest_in_range] + 1, y + 1,
                                 zbeg, r };
          }
        }
      }
    }
  }
  if(maxvolume != -1) {
    for(int x = maxrange.xbeg; x < maxrange.xend; ++x) {
      for(int y = maxrange.ybeg; y < maxrange.yend; ++y) {
        for(int z = maxrange.zbeg; z < maxrange.zend; ++z) {
          remain_.erase(Vec3{x, y, z});
        }
      }
    }
  }
  return maxrange;
}

std::ostream& operator << (std::ostream &os, const BoxRange &br)
{
  os << "{ [" << br.xbeg << ", " << br.xend
     << "), [" << br.ybeg << ", " << br.yend
     << "), [" << br.zbeg << ", " << br.zend << ") }";
  return os;
}

