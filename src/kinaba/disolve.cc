#include "disolve.h"

#include <iostream>
#include <algorithm>
#include <map>
#include <cassert>
#include "../psh/state.h"

namespace {
int ToIndex(const Vec3& pos, int r) {
  return pos.x * r * r + pos.y * r + pos.z;
}

}

std::vector<Command> ReverseTrace(int R, const std::vector<Command>& commands) {
  std::vector<std::vector<std::pair<int,Command>>> reverse_commands;
  {
  Model model;
  model.SetEmptyModel(R);
  State state(nullptr, &model, commands);

  for(;;) {
    // Pre-step information
    size_t cmd_beg = state.current_trace();
    const std::vector<Bot> pre_bots = state.bots();

    // Done
    if(cmd_beg+1 == commands.size() && commands[cmd_beg].type()==Command::Type::Halt)
      break;

    // Execute a step
    if(!state.ExecuteStep()) {
      std::cerr << "[ReverseTrace] cannot reverse an incomplete solution." << std::endl;
      return std::vector<Command>();
    }

    // Reverse each command. Each bot is identified by its position,
    // and the command ordering by id is adjusted later.
    std::vector<std::pair<int,Command>> cur;
    for(size_t i=cmd_beg; i!=cmd_beg+pre_bots.size(); ++i) {
      const Bot& pre_bot = pre_bots[i - cmd_beg];

      switch(commands[i].type()) {
      case Command::Type::Halt: {
        std::cerr << "[ReverseTrace] unexpected halt at trace " << i << std::endl;
        return std::vector<Command>();
      } break;
      case Command::Type::Wait: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R), Command::Wait());
      } break;
      case Command::Type::Flip: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R), Command::Flip());
      } break;
      case Command::Type::SMove: {
        cur.emplace_back(ToIndex(pre_bot.pos() + commands[i].arg1(), R),
                         Command::SMove(-commands[i].arg1()));
      } break;
      case Command::Type::LMove: {
        cur.emplace_back(
            ToIndex(pre_bot.pos() + commands[i].arg1() + commands[i].arg2(), R),
            Command::LMove(-commands[i].arg2(), -commands[i].arg1()));
      } break;
      case Command::Type::FusionP: {
        int before_seeds = pre_bot.seeds().size();
        int after_seeds = -1;
        for(const Bot& post_bot: state.bots())
          if(pre_bot.bid() == post_bot.bid())
            after_seeds = post_bot.seeds().size();
        assert(after_seeds != -1);
        cur.emplace_back(ToIndex(pre_bot.pos(), R),
                         Command::Fission(commands[i].arg1(),
                                          after_seeds - before_seeds - 1));
      } break;
      case Command::Type::FusionS: {
        // The bot just disappears.
      } break;
      case Command::Type::Fission: {
        const Vec3 v = commands[i].arg1();
        cur.emplace_back(ToIndex(pre_bot.pos(), R), Command::FusionP(v));
        cur.emplace_back(ToIndex(pre_bot.pos()+v, R), Command::FusionS(-v));
      } break;
      case Command::Type::Fill: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R),
                         Command::Void(commands[i].arg1()));
      } break;
      case Command::Type::Void: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R),
                         Command::Fill(commands[i].arg1()));
      } break;
      case Command::Type::GFill: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R),
                         Command::GVoid(commands[i].arg1(), commands[i].arg2()));
      } break;
      case Command::Type::GVoid: {
        cur.emplace_back(ToIndex(pre_bot.pos(), R),
                         Command::GFill(commands[i].arg1(), commands[i].arg2()));
      } break;
      }
    }

    // Done
    reverse_commands.emplace_back(cur);
  }
  }

  std::vector<Command> result;
  {
    // Reverse play to compute the id<=>pos mapping.
    Model model;
    model.SetEmptyModel(R);
    State retro(nullptr, &model, std::vector<Command>());

    bool bug = false;
    for_each(reverse_commands.rbegin(), reverse_commands.rend(),
      [&](const std::vector<std::pair<int,Command>>& step) {
          if(bug) return;
          const size_t pre_size = result.size();

          std::map<int, int> pos2id;
          for(auto&& bot: retro.bots())
            pos2id[ToIndex(bot.pos(), R)] = bot.bid();

          std::map<int, Command> id2cmd_sorted;
          for(auto&& it: step)
            id2cmd_sorted[pos2id[it.first]] = it.second;

          for(auto&& it: id2cmd_sorted)
            result.emplace_back(it.second);

          retro.AppendTrace(result.begin()+pre_size, result.end());
          if(!retro.ExecuteStep()) {
            std::cerr << "[ReverseTrace] retro execution failed. something is wrong" << std::endl;
            bug = true;
          }
       });
  }
  result.emplace_back(Command::Halt());
  return result;
}
