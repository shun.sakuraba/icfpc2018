#include "ki_solver.h"

#include <set>
#include <map>
#include <queue>
#include <iostream>
#include "../psh/command_util.h"
#include "../psh/ufmatrix.h"
#include "../psh/model_util.h"

KiSolver::KiSolver(const Model* model) : model_(model) {}

std::vector<Command> KiSolver::Solve(int maxP) {
  std::vector<Command> result;

  const int R = model_->r();
  int P = std::min(maxP, R*2-1);

  // Set of to be filled cells.
  std::set<Vec3> todo;
  for(int x=0; x<R; ++x)
  for(int y=0; y<R; ++y)
  for(int z=0; z<R; ++z) {
    Vec3 v = {x,y,z};
    if(model_->filled(v))
      todo.emplace(v);
  }

std::cerr<<"@R="<<R<<" @T="<<todo.size()<<std::endl;
if(R>80 || (R>50 && todo.size()>2000)) return std::vector<Command>();
if(R>25 && P!=8) return std::vector<Command>();



  // Scatter P drones (TODO: logstep) (TODO: R<10 map)
  std::vector<Vec3> pos = {{0,0,0}};
  for(int p=1; p<P; ++p) {
    for(int _=0; _<p-1; ++_)
      result.emplace_back(Command::Wait());
    int dx=p&1, dz=dx^1;
    result.emplace_back(Command::Fission(dx,0,dz,39-p));
    pos.emplace_back(pos.back() + Vec3{dx,0,dz});
  }

  // Run the main loop
  UFMatrix ufm(R);
  int filled_max_y = 0;

  int stuck = 0;
  for(;;) {
//    std::cerr<<"@"<<todo.size()<<std::endl;

    if(todo.empty() && P==1 && pos[0]==Vec3{0,0,0}) {
      result.emplace_back(Command::Halt());
      break;
    }

    if(stuck >= 2 || todo.empty()) {
      // MERGE MODE ////////////////
      bool merge_happening = false;
      std::vector<Command> cmds(P, Command::Wait());
      std::vector<int> ps;
      for(int p=0; p<P; ++p) ps.push_back(p);
      sort(ps.begin(), ps.end(), [&](int p, int q){
        return pos[p].y > pos[q].y;
      });

      for(int pi=0; pi<P; ++pi)
      for(int qi=pi+1; qi<P; ++qi) {
        int q=ps[pi], p=ps[qi]; //!!keep lower(q)
        if(IsNear(pos[p]-pos[q])) {
          merge_happening = true;
          Vec3 d = pos[q] - pos[p];
          cmds[p] = Command::FusionP(d.x, d.y, d.z);
          cmds[q] = Command::FusionS(-d.x, -d.y, -d.z);
          pos.erase(pos.begin()+q);
          --P;
          goto pair_found; // TODO: more at the same time.
        }
      }
      pair_found:
      if(merge_happening) {
        stuck = 0;
        result.insert(result.end(), cmds.begin(), cmds.end());
        continue;
      }
      //////////////////////////////
      if(!todo.empty() && stuck >= 10) {
        std::set<Vec3> botpos(pos.begin(), pos.end());
        for(int y=model_->max_y(); y>=0; --y) {
          std::cerr<<"["<<y<<"]"<<std::endl;
          for(int z=model_->max_z()+1; z>=0; --z) {
            for(int x=0; x<model_->max_x()+2; ++x) {
              Vec3 v = {x,y,z};
              if(botpos.count(v) && todo.count(v)) std::cerr<<'#';
              else if(botpos.count(v)) std::cerr<<'+';
              else if(todo.count(v)) std::cerr<<'-';
              else if(model_->filled(v)) std::cerr<<'*';
              else std::cerr<<'.';
            }
            std::cerr<<std::endl;
          }
        }
        std::cerr << "@@@ " << todo.size() << " cells left. @@@" << std::endl;
        break;
      }
      if(todo.empty() && stuck >= 1000) {
        std::cerr << "@@@ " << P << " bots left, failed to merge. @@@" << std::endl;
        break;
      }
    }

    std::set<Vec3> target_set;
    std::vector<Vec3> tofill;
    {
      auto blocked = [&](const Vec3& v) {
        if(!v.IsValid(R)) return true;
        if(ufm.IsFull(v)) return true;
        return false;
      };

      if(todo.empty()) {
        target_set = std::set<Vec3>{{0,0,0}, {0,0,1}, {0,1,0}, {1,0,0},
            {1,1,0}, {0,1,1}, {1,0,1}};
      } else {
        std::map<int,std::vector<Vec3>> y2vs;
        for(auto&& v: todo)
          y2vs[v.y].emplace_back(v);

        for(auto&& y2vs_elem: y2vs) {
          for(auto&& v: y2vs_elem.second) if(ufm.IsAdjacentToGrounded(v)) { // TODO: ufm_next
            tofill.emplace_back(v);
            for(int dy=+1; dy>=(stuck?-1:+1); --dy)
            for(int dx=-1; dx<=+1; ++dx)
            for(int dz=-1; dz<=+1; ++dz) {
              Vec3 d = {dx,dy,dz};
              if(IsNear(d) && !blocked(v+d))
                target_set.emplace(v+d);
            }
          }
          if(tofill.size() >= (stuck?1:P)) break;
        }
      }
    }
    int bottomY = R;
    for(auto&& v: target_set)
      bottomY = std::min(bottomY, v.y);
    for(auto&& v: pos)
      bottomY = std::min(bottomY, v.y);

    int non_wait = 0;
    std::set<Vec3> volat(pos.begin(), pos.end());
    for(int p=0; p<P; ++p) {
      // !!!!! BFS !!!!!!
      auto blocked = [&](const Vec3& v) {
        if(v == pos[p]) return false;
        if(v.x<0 || v.y<0 || v.z<0) return true;
        if(v.x>model_->max_x()+1 || v.z>model_->max_z()+1) return true;
        if(v.y>filled_max_y+1) return true;
        if(!stuck && v.y<bottomY-1) return true;
        if(ufm.IsFull(v)) return true;
        if(volat.count(v)) return true;
        return false;
      };

      // init
      std::queue<Vec3> q;
      std::map<Vec3,Vec3> action;
      for(auto& t: target_set) if(!blocked(t)) {
        q.emplace(t);
        action[t] = {0,0,0};
        if(todo.empty()) break;
      }

      // loop
      while(!q.empty() && !action.count(pos[p])) {
        Vec3 v = q.front(); q.pop();

        std::vector<Vec3> dirs = {{1,0,0}, {-1,0,0}, {0,1,0}, {0,-1,0}, {0,0,1}, {0,0,-1}};
        std::vector<bool> dir_blocked(dirs.size(), false);
        for(int d=+1; d<=+15; ++d)
          for(int i=0; i<dirs.size(); ++i) {
            if(dir_blocked[i]) continue;
            auto& dir = dirs[i];

            Vec3 u = v + dir*d;
            if(blocked(u)) {dir_blocked[i]=true; continue;}
            if(action.count(u)) continue;
            q.emplace(u);
            action[u] = v-u;
          }
      }

      // Action
      Vec3 act = action.count(pos[p]) ? action[pos[p]] : Vec3{0,0,0};
      if(act.x||act.y||act.z) {
        ++non_wait;

        // SMove
        int len = CLen(act);
        Vec3 dir = act / len;
        for(int _=0; _<len; ++_)
          volat.emplace(pos[p]+=dir);

        // LMove
        if(MLen(act)<=5 && action.count(pos[p]) && MLen(action[pos[p]])>0 && MLen(action[pos[p]])<=5) {
          Vec3 act2 = action[pos[p]];
          int len = CLen(act2);
          Vec3 dir = act2 / len;
          for(int _=0; _<len; ++_)
            volat.emplace(pos[p]+=dir);
          result.emplace_back(Command::LMove(act, act2));
//std::cerr<<"["<<p<<"] LMove"<<act<<" "<<act2<<std::endl;
        } else {
          result.emplace_back(Command::SMove(act));
//std::cerr<<"["<<p<<"] SMove"<<act<<std::endl;
        }
      } else {
        bool fired = false;

        // Fire
        int acc = 0;
        for(auto&& v: tofill)
          if(IsNear(v-pos[p]) && !blocked(v) && ufm.IsAdjacentToGrounded(v)) {
            if(!stuck && v.y>=pos[p].y) continue; // only shoot below
//std::cerr<<"["<<p<<"] Fill "<<v<<std::endl;
            ++non_wait;
            result.emplace_back(Command::Fill(v-pos[p]));
            volat.emplace(v);
            ufm.Add(v);
            filled_max_y = std::max(filled_max_y, v.y);
            todo.erase(v);

            fired = true;
            break;
          }

        // Wait
        if(!fired) {
          result.emplace_back(Command::Wait());
//std::cerr<<"["<<p<<"] Wait ac="<<action.count(pos[p])<<std::endl;
        }
      }
      /////////////////////
    }
 
    stuck = (non_wait==0 ? stuck+1 : 0);
    if(non_wait == 0)
      result.resize(result.size() - P); 
  }

  std::cerr << "@DONE" << std::endl;
  return result;
}
