#ifndef DISOLVE_H_
#define DISOLVE_H_

#include <vector>
#include "../psh/model.h"
#include "../psh/command.h"

std::vector<Command> ReverseTrace(int R, const std::vector<Command>& commands);

template <typename Solver>
std::vector<Command> SolveByReversal(const Model* model) {
  Solver solver(model);
  return ReverseTrace(model->r(), solver.Solve());
}

#endif  // DISOLVE_H_
