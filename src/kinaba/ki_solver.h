#ifndef KI_SOLVER_H_
#define KI_SOLVER_H_

#include "../psh/model.h"
#include "../psh/command.h"

class KiSolver {
 public:
  explicit KiSolver(const Model* model);

  std::vector<Command> Solve(int maxP);

 private:
  const Model* const model_;

  KiSolver(const KiSolver&) = delete;
  KiSolver& operator=(const KiSolver&) = delete;
};

#endif  // KI_SOLVER_H_
